Pod::Spec.new do |s|
  s.name         = "NKKit"
  s.version      = "1.0.0"
  s.summary      = "Nick5683 kit."
  s.homepage     = "https://gitee.com/nick5683/NKKit.git"
  s.license      = "MIT"
  s.author       = { "Nick" => "157132026@qq.com" }
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://gitee.com/nick5683/NKKit.git", :tag => s.version.to_s }
  s.requires_arc = true
  s.swift_version = '5.0'
  
  

  s.resource_bundles = {
    'NKKit' => ['NKKit/Assets/*.png']
  }
       
  # 核心文件
  s.subspec 'Core' do |ss|
    ss.source_files = "NKKit/Classes/NKKit.swift"
  end

  # 基础包
  s.subspec 'Base' do |ss|
    ss.source_files = "NKKit/Classes/Base/*.swift"
    ss.dependency "MJRefresh"
  end

  # 扩展方法
  s.subspec 'Categories' do |ss|
      ss.source_files = "NKKit/Classes/Categories/**/*.swift"
    ss.dependency "NKKit/Core"
  end

  # 通用Views
  s.subspec 'Views' do |ss|
    ss.source_files = "NKKit/Classes/Views/**/*.swift"
  end

  # 导航栏
  s.subspec 'NavigationBar' do |ss|
    ss.source_files = "NKKit/Classes/Components/NavigationBar/**/*.swift"
    ss.dependency "NKKit/Core"
  end

  # HUD
  s.subspec 'ProgressHUD' do |ss|
    ss.source_files = "NKKit/Classes/Components/ProgressHUD/*.swift"
    ss.dependency "NKKit/Core"
    ss.dependency "MBProgressHUD"
  end

  # 宫格图片展示/操作
  s.subspec 'ImageGridView' do |ss|
    ss.source_files = "NKKit/Classes/Components/ImageGridView/**/*.swift"
    ss.dependency "Kingfisher"
  end

  # 全屏图片浏览器
  s.subspec 'ImageBrowser' do |ss|
    ss.source_files = "NKKit/Classes/Components/ImageBrowser/**/*.swift"
    ss.dependency "Kingfisher"
  end

  
  # 图片/视频选择
  s.subspec 'MediaPicker' do |ss|
    ss.source_files = "NKKit/Classes/Components/MediaPicker/**/*.swift"
    ss.dependency "TZImagePickerController"
  end

  # WebViewController
  s.subspec 'WebViewController' do |ss|
    ss.source_files = "NKKit/Classes/Components/WebViewController/**/*.swift"
  end

  # Codable扩展
  s.subspec 'Codable' do |ss|
    ss.source_files = "NKKit/Classes/Components/Codable/**/*.swift"
  end

  # 角标视图
  s.subspec 'BadgeView' do |ss|
    ss.source_files = "NKKit/Classes/Components/BadgeView/**/*.swift"
    ss.dependency "SnapKit"
  end

  # 消息提示
  s.subspec 'MessageHUD' do |ss|
    ss.source_files = "NKKit/Classes/Components/MessageHUD/*.swift"
  end
  
  # tab分页视图
  s.subspec 'SegmentedView' do |ss|
    ss.source_files = "NKKit/Classes/Components/SegmentedView/*.swift"
    ss.dependency "NKKit/Views"
    ss.dependency "JXSegmentedView"
  end

  # 工具类
  s.subspec 'Utils' do |ss|
    ss.source_files = "NKKit/Classes/Components/Utils/*.swift"
    ss.dependency "NKKit/Categories"
  end

  # 弹出视图控制器
  s.subspec 'OverlayController' do |ss|
    ss.source_files = "NKKit/Classes/Components/OverlayController/**/*.swift"
  end

  # 引用自IBAnimatable库的部分内容
  s.subspec 'IBAnimatable' do |ss|
    ss.source_files = "NKKit/Classes/Components/IBAnimatable/**/*.swift"
  end

   

end
