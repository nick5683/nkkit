//
//  NKKit.swift
//
//  Created by Nick on 2019/11/12.
//

import Foundation

public final class NKFramework<Base> {
    public let base: Base
    
    public init(_ base: Base) {
        self.base = base;
    }
}

public protocol NKKitNamespaceWrappable {
    associatedtype WrapperType
    
    static var nk: NKFramework<WrapperType>.Type { get }
    var nk: NKFramework<WrapperType> { get }
}


extension NKKitNamespaceWrappable {
    public static var nk: NKFramework<Self>.Type {
        get { return NKFramework<Self>.self }
    }
    
    public var nk: NKFramework<Self> {
        get { return NKFramework(self) }
    }
}

extension NSObject : NKKitNamespaceWrappable { }
