//
//  CheckBoxButton.swift
//
//  Created by Nick on 2020/3/16.
//

import UIKit

public class CheckBoxButton: UIButton {
    /// 正常状态图片
    @IBInspectable public dynamic var normalImage: UIImage? {
        didSet { setImage(normalImage, for: .normal) }
    }

    /// 选中状态图片
    @IBInspectable public dynamic var selectedImage: UIImage? {
        didSet { setImage(selectedImage, for: .selected) }
    }

    // MARK: - Events

    @objc func tappedAction() {
        isSelected = !isSelected
        sendActions(for: .valueChanged)
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        initSubviews()
    }

    func initSubviews() {
        addTarget(self, action: #selector(tappedAction), for: .touchUpInside)
        setImage(normalImage, for: .normal)
        setImage(selectedImage, for: .selected)
    }
}
