//
//  Button.swift
//
//  Created by Nick on 2019/11/21.
//

import UIKit

public class DWButton: UIButton {
    /// 图片位置
    public var imageMode: ImageMode = .top
    /// 线条方向，XIB/SB属性
    @IBInspectable public var ib_imageMode: String = ImageMode.top.rawValue {
        didSet { imageMode = ImageMode(rawValue: ib_imageMode) ?? .top }
    }

    /// 图文间距
    @IBInspectable public var imageSpacing: CGFloat = 5

    private var contentSize: CGSize = .zero

    public enum ImageMode: String {
        case top
        case bottom
        case left
        case right
    }

    // MARK: - Override

    public override func layoutSubviews() {
        super.layoutSubviews()
        guard let titleLabel = titleLabel, let imageView = imageView else { return }

        let titleW = titleLabel.bounds.width
        let titleH = titleLabel.bounds.height

        let imageW = imageView.bounds.width
        let imageH = imageView.bounds.height

        let btnCenterX = bounds.width * 0.5
        let imageCenterX = btnCenterX - titleW * 0.5
        let titleCenterX = btnCenterX + imageW * 0.5
        let spacing = imageSpacing

        var width: CGFloat = 0
        var height: CGFloat = 0

        switch imageMode {
        case .top:
            titleEdgeInsets = UIEdgeInsets(top: imageH * 0.5 + spacing * 0.5, left: -(titleCenterX - btnCenterX), bottom: -(imageH * 0.5 + spacing * 0.5), right: titleCenterX - btnCenterX)
            imageEdgeInsets = UIEdgeInsets(top: -(titleH * 0.5 + spacing * 0.5), left: btnCenterX - imageCenterX, bottom: titleH * 0.5 + spacing * 0.5, right: -(btnCenterX - imageCenterX))
        case .bottom:
            titleEdgeInsets = UIEdgeInsets(top: -(imageH * 0.5 + spacing * 0.5), left: -(titleCenterX - btnCenterX), bottom: imageH * 0.5 + spacing * 0.5, right: titleCenterX - btnCenterX)
            imageEdgeInsets = UIEdgeInsets(top: titleH * 0.5 + spacing * 0.5, left: btnCenterX - imageCenterX, bottom: -(titleH * 0.5 + spacing * 0.5), right: -(btnCenterX - imageCenterX))
        case .left:
            titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing * 0.5, bottom: 0, right: -(spacing * 0.5))
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -(spacing * 0.5), bottom: 0, right: spacing * 0.5)
        case .right:
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -(imageW + spacing * 0.5), bottom: 0, right: imageW + spacing * 0.5)
            imageEdgeInsets = UIEdgeInsets(top: 0, left: titleW + spacing * 0.5, bottom: 0, right: -(titleW + spacing * 0.5))
        }

        if imageMode == .top || imageMode == .bottom {
            width = max(titleLabel.frame.width, imageView.frame.width)
            height = titleLabel.frame.height + spacing + imageView.frame.height
        } else {
            width = titleLabel.frame.width + spacing + imageView.frame.width
            height = max(titleLabel.frame.height, imageView.frame.height)
        }
        if width != contentSize.width || height != contentSize.height {
            contentSize = CGSize(width: max(width, contentSize.width), height: max(height, contentSize.height))
        }
        if !frame.size.equalTo(contentSize) {
            contentSize = CGSize(width: max(frame.width, contentSize.width), height: max(frame.height, contentSize.height))
            invalidateIntrinsicContentSize()
        }
    }

    public override var intrinsicContentSize: CGSize {
        if contentSize == .zero { return super.intrinsicContentSize }
        return contentSize
    }
}
