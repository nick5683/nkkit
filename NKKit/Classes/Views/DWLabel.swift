//
//  DWLabel.swift
//
//  Created by Nick on 2019/11/22.
//

import UIKit

class CustomPaddingLabel: UILabel {
    
    var textInsets: UIEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
    
    override func drawText(in rect: CGRect) {
        let newRect = rect.inset(by: textInsets)
        super.drawText(in: newRect)
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insets = textInsets
        let newRect = bounds.inset(by: insets)

        var rect = super.textRect(forBounds: newRect,
                                  limitedToNumberOfLines: numberOfLines)
        
        rect.origin.x -= insets.left
        rect.origin.y -= insets.top
        rect.size.width += (insets.left + insets.right)
        rect.size.height += (insets.top + insets.bottom)
        return rect
    }
    
}
