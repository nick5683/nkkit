//
//  AutoHeightWebView.swift
//
//  Created by Nick on 2019/11/26.
//

import UIKit
import WebKit

public class AutoHeightWebView: UIView, WKNavigationDelegate {
    private var contentView: WKWebView!
    private var indicatorView: UIActivityIndicatorView!
    public private(set) var contentSize: CGSize = .zero
    public var didUpdateContentSize: (() -> Void)?

    public func loadURL(_ url: URL) {
        contentView.isHidden = true
        indicatorView.startAnimating()
        contentView.load(URLRequest(url: url))
    }

    public func loadHTMLString(_ string: String) {
        contentView.isHidden = true
        indicatorView.startAnimating()
        contentView.loadHTMLString(string, baseURL: Bundle.main.bundleURL)
    }

    // MARK: - KVO

    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            let oldSize = contentSize
            contentSize = change?[NSKeyValueChangeKey.newKey] as! CGSize
            if oldSize.width != contentSize.width || oldSize.height != contentSize.height {
                invalidateIntrinsicContentSize()
                didUpdateContentSize?()
            }
        }
    }

    // MARK: - WKNavigationDelegate

    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        contentView.isHidden = false
        indicatorView.stopAnimating()
    }

    // MARK: - Override

    public override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = bounds
        indicatorView.sizeToFit()
        indicatorView.center = CGPoint(x: bounds.width * 0.5, y: bounds.height * 0.5)
    }

    public override var intrinsicContentSize: CGSize {
        var size = contentSize
        size.height = size.height == 0 ? 50 : size.height
        return size
    }

    deinit {
        contentView.scrollView.removeObserver(self, forKeyPath: "contentSize")
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        initSubviews()
    }

    func initSubviews() {
        let config = WKWebViewConfiguration()
        config.userContentController = WKUserContentController()
        let jsStr = """
        var script = document.createElement('meta');
        script.name = 'viewport';
        script.content="width=device-width, initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0, user-scalable=no";
        document.getElementsByTagName('head')[0].appendChild(script);
        document.documentElement.style.webkitTouchCallout='none';
        document.documentElement.style.webkitUserSelect='none';
        """
        let wkScript = WKUserScript(source: jsStr, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        config.userContentController.addUserScript(wkScript)
        contentView = WKWebView(frame: bounds, configuration: config)
        contentView.isHidden = true
        contentView.navigationDelegate = self
        contentView.scrollView.isScrollEnabled = false
        contentView.scrollView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        addSubview(contentView)

        indicatorView = UIActivityIndicatorView()
        indicatorView.hidesWhenStopped = true
        addSubview(indicatorView)
    }
}
