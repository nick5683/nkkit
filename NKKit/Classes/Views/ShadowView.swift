//
//  ShadowView.swift
//
//  Created by Nick on 2019/11/13.
//

import UIKit

public class ShadowView: UIView {
    /// 阴影大小
    @IBInspectable public var shadowRadius: CGFloat = 0 {
        didSet { layer.shadowRadius = shadowRadius }
    }
    /// 阴影透明度
    @IBInspectable public var shadowOpacity: Float = 0 {
        didSet { layer.shadowOpacity = shadowOpacity }
    }
    /// 阴影偏移
    @IBInspectable public var shadowOffset: CGSize = .zero {
        didSet { layer.shadowOffset = shadowOffset }
    }
    /// 阴影颜色
    @IBInspectable public var shadowColor: UIColor? {
        didSet { layer.shadowColor = shadowColor?.cgColor }
    }
    
    // MARK: - Override
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if shadowOpacity > 0 && shadowColor != nil {
            let path = UIBezierPath(roundedRect: bounds, cornerRadius: shadowRadius)
            layer.shadowPath = path.cgPath
        }
    }
}
