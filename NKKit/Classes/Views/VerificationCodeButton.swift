//
//  VerificationCodeButton.swift
//
//  Created by Nick on 2019/11/13.
//

import UIKit

public class VerificationCodeButton: UIButton {
    /// 倒计时总时间，default = 60s
    @IBInspectable public var countdownTime: Int = 60
    /// 倒计时进行时标题，默认 = 秒
    @IBInspectable public var countdowningTitle: String = "秒"
    /// 是否正在进行倒计时
    public private(set) var isCountdowning: Bool = false
    /// 倒计时标题格式化，如果设置此属性countdowningTitle无效
    public var countdowningTitleFormatter: ((_ remainTime: Int) -> String)?

    var timer: Timer?
    var remainTime: Int = 0
    var rawTitle: String?

    // MARK: - Public

    public func startCountdown() {
        if isCountdowning { return }
        isEnabled = false
        isCountdowning = true
        remainTime = countdownTime
        rawTitle = title(for: .normal)
        timer = Timer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: .common)
        timer?.fire()
    }

    public func stopCountdown() {
        if !isCountdowning { return }
        isEnabled = true
        isCountdowning = false
        updateTitle(title: rawTitle)
        timer?.invalidate()
        timer = nil
    }

    // MARK: - Events

    @objc func timerAction() {
        var title: String
        if let formatter = countdowningTitleFormatter {
            title = formatter(remainTime)
        } else {
            title = "\(remainTime)\(countdowningTitle)"
        }
        updateTitle(title: title)
        remainTime -= 1
        if remainTime <= 0 { stopCountdown() }
    }

    func updateTitle(title: String?) {
        titleLabel?.text = title
        setTitle(title, for: .normal)
    }
}
