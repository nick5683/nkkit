//
//  PasswordTextField.swift
//
//  Created by Nick on 2019/11/15.
//

import UIKit

public class PasswordTextField: NKTextField {
    var rightButton: UIButton = UIButton(type: .custom)
    /// 显示安全字符状态图片
    @IBInspectable public var secureImage: UIImage? {
        didSet {
            rightButton.setImage(secureImage, for: .normal)
        }
    }
    /// 显示明文字符状态图片
    @IBInspectable public var unsecureImage: UIImage? {
        didSet {
            rightButton.setImage(unsecureImage, for: .selected)
        }
    }
    /// 安全字符指示图宽度
    @IBInspectable public var secureImageWidth: CGFloat = 0 {
        didSet {
            rightButton.frame = CGRect(x: 0, y: 0, width: secureImageWidth, height: bounds.height)
            rightView = rightButton
        }
    }

    public override var isSecureTextEntry: Bool {
        set {
            super.isSecureTextEntry = newValue
            rightButton.isSelected = !newValue
        }
        get { return super.isSecureTextEntry }
    }

    // MARK: - Events

    @objc func onRightButtonClick() {
        isSecureTextEntry = !isSecureTextEntry
    }
    
    // MARK: - Override
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        rightView?.frame = CGRect(x: bounds.width - secureImageWidth, y: 0, width: secureImageWidth, height: bounds.height)
    }

    // MARK: - Init

    public override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        initSubviews()
    }

    func initSubviews() {
        isSecureTextEntry = true

        rightButton.frame = CGRect(x: 0, y: 0, width: secureImageWidth, height: bounds.height)
        rightButton.setImage(secureImage, for: .normal)
        rightButton.setImage(unsecureImage, for: .selected)
        rightButton.addTarget(self, action: #selector(onRightButtonClick), for: .touchUpInside)
        rightView = rightButton
        rightViewMode = .always
    }
}
