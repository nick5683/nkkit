//
//  PrimaryButton.swift
//
//  Created by Nick on 2019/11/15.
//

import UIKit

public class PrimaryButton: UIButton {
    /// 背景色
    @objc public dynamic var primayColor: UIColor? {
        didSet {
            if primayColor != nil {
                setBackgroundImage(UIImage.createImage(color: primayColor!), for: .normal)
            } else {
                setBackgroundImage(nil, for: .normal)
            }
        }
    }

    /// 禁用状态背景色
    @objc public dynamic var disableColor: UIColor? {
        didSet {
            if disableColor != nil {
                setBackgroundImage(UIImage.createImage(color: disableColor!), for: .disabled)
            } else {
                setBackgroundImage(nil, for: .disabled)
            }
        }
    }

    /// 圆角大小
    @objc public dynamic var cornerRadius: CGFloat = 0 {
        didSet { layer.cornerRadius = cornerRadius }
    }

    /// 标题颜色
    @objc public dynamic var titleColor: UIColor? {
        didSet { setTitleColor(titleColor, for: .normal) }
    }

    /// 禁用状态标题颜色
    @objc public dynamic var disableTitleColor: UIColor? {
        didSet { setTitleColor(titleColor, for: .disabled) }
    }

    /// 标题字体
    @objc public dynamic var titleFont: UIFont? = UIFont.systemFont(ofSize: 14) {
        didSet { titleLabel?.font = titleFont }
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
        initSubviews()
    }

    func initSubviews() {
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        tintColor = .white
        titleLabel?.font = titleFont
        setTitleColor(titleColor, for: .normal)
        setTitleColor(disableTitleColor, for: .disabled)
        if primayColor != nil {
            setBackgroundImage(UIImage.createImage(color: primayColor!), for: .normal)
        }
        if disableColor != nil {
            setBackgroundImage(UIImage.createImage(color: disableColor!), for: .disabled)
        }
    }
}
