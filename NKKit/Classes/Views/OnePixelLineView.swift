//
//  DWOnePixelLineView.swift
//
//  Created by Nick on 2019/11/13.
//

import UIKit

public class OnePixelLineView: UIView {
    /// 线条颜色
    @IBInspectable public dynamic var lineColor: UIColor? {
        didSet { (layer as! CAShapeLayer).strokeColor = lineColor?.cgColor }
    }

    /// 线条宽度
    @IBInspectable public var lineWidth: CGFloat = 1 {
        didSet { (layer as! CAShapeLayer).lineWidth = lineWidth / UIScreen.main.scale }
    }

    /// 线条方向
    public var direction: Direction = .horizontal
    /// 线条方向，XIB/SB属性
    @IBInspectable public var ib_direction: String = "horizontal" {
        didSet { direction = Direction(rawValue: ib_direction) ?? .horizontal }
    }

    /// 线条位置
    public var location: Location = .center
    /// 线条位置，XIB/SB属性
    @IBInspectable public var ib_location: String = "center" {
        didSet { location = Location(rawValue: ib_location) ?? .center }
    }

    /// 虚线样式
    public var lineDashPattern: Array<NSNumber>? {
        didSet {
            (layer as! CAShapeLayer).lineDashPattern = lineDashPattern
        }
    }

    /// 虚线样式，XIB/SB属性
    @IBInspectable public var ib_lineDashPattern: String? {
        didSet {
            guard let pattern = ib_lineDashPattern else { return }
            let numbers = pattern.components(separatedBy: ",")
            if let width = Float(numbers[0]),
                let spacing = Float(numbers[1]) {
                lineDashPattern = [NSNumber(value: width), NSNumber(value: spacing)]
            } else {
                lineDashPattern = nil
            }
        }
    }

    public enum Direction: String {
        // 纵向
        case vertical
        // 横向
        case horizontal
    }

    public enum Location: String {
        case center
        case top
        case bottom
        case left
        case right
    }

    // MARK: - Override

    public override class var layerClass: AnyClass { return CAShapeLayer.self }

    public override func layoutSubviews() {
        super.layoutSubviews()
        var lineS: CGFloat = 0
        let path = UIBezierPath()
        let layer = self.layer as! CAShapeLayer
        if direction == .vertical {
            if location == .left {
                lineS = layer.lineWidth * 0.5
            } else if location == .right {
                lineS = bounds.width - lineWidth + layer.lineWidth * 0.5
            } else {
                lineS = bounds.width * 0.5
            }
            path.move(to: CGPoint(x: lineS, y: 0))
            path.addLine(to: CGPoint(x: lineS, y: bounds.maxY))
        } else {
            if location == .top {
                lineS = layer.lineWidth * 0.5
            } else if location == .bottom {
                lineS = bounds.height - layer.lineWidth + layer.lineWidth * 0.5
            } else {
                lineS = bounds.height * 0.5
            }
            path.move(to: CGPoint(x: 0, y: lineS))
            path.addLine(to: CGPoint(x: bounds.maxX, y: lineS))
        }
        layer.path = path.cgPath
    }

    public override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        // 触发 UIAppearance，避免代码设置lineColor被全局设置覆盖
        if lineColor != nil {
            let color = lineColor
            lineColor = color
        }
    }

    // MAKR: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSubviews()
    }

    func initSubviews() {
        backgroundColor = UIColor.clear
        let layer = self.layer as! CAShapeLayer
        layer.strokeColor = lineColor?.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineWidth = lineWidth / UIScreen.main.scale
    }
}
