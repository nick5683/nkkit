//
//  TextFiled.swift
//
//  Created by Nick on 2019/11/15.
//

import UIKit

public class NKTextField: UITextField {
    public var inputFormatters: Array<TextInputFormatter> = []
    public var textChanged: ((String)->())?

    public override var text: String? {
        set { super.text = format(value: newValue) }
        get { return super.text }
    }
    
    // MARK: - Events
    
    @objc func onValueChanged() {
        // 输入中文状态，会存在待确定文本，不处理长度
        if markedTextRange != nil { return }
        super.text = format(value: text)
    }
    
    func format(value: String?) -> String? {
        var newValue = value
        inputFormatters.forEach { (f) in
            newValue = f.format(value: newValue)
        }
        textChanged?(newValue ?? "")
        return newValue
    }
    
    // MARK: - Init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        initEvents()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initEvents()
    }
    
    func initEvents() {
        addTarget(self, action: #selector(onValueChanged), for: .editingChanged)
    }
}

public protocol TextInputFormatter {
    func format(value: String?) -> String?
}

public class WhitelistingTextInputFormatter: TextInputFormatter {
    /// 正则表达式
    var regExp: String

    public init(regExp: String) {
        self.regExp = regExp
    }

    public func format(value: String?) -> String? {
        guard let val = value else { return nil }
        do {
            let regular = try NSRegularExpression(pattern: regExp, options: .caseInsensitive)
            let res = regular.matches(in: val, options: .init(rawValue: 0), range: NSRange(location: 0, length: val.count))
            var resultStr: String = ""
            for r in res {
                resultStr += (val as NSString).substring(with: r.range)
            }
            return resultStr
        } catch {
            print(error)
            return nil
        }
    }
}

public class LengthLimitingTextInputFormatter: TextInputFormatter {
    var maxLenght: Int

    public init(_ maxLenght: Int) {
        self.maxLenght = maxLenght
    }

    public func format(value: String?) -> String? {
        guard let val = value else { return nil }
        if val.count > maxLenght {
            return String(val.prefix(maxLenght))
        }
        return val
    }
}
