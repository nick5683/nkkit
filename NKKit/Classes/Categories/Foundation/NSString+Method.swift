//
//  NK_NSString.swift
//  NKKit
//
//  Created by dhcc on 2023/11/30.
//

import UIKit


//自定义属性
public extension String{
    
    ///字符串自身 长度
    var length: Int {
        get{
            return self.count
        }
    }
    
    ///转换为int类型
    var int : Int {
        return NSString(string: self).integerValue
    }
    
    ///转换为int32类型
    var int32 : Int32 {
        return NSString(string: self).intValue
    }
    
    ///转换为int64类型
    var int64 : Int64 {
        return NSString(string: self).longLongValue
    }
    
    ///转换为Double类型
    var doubleValue : Double {
        if let float = Double(self){
            return float
        }
        return 0
    }
    
    ///转换为CGFloat类型
    var floatValue : CGFloat {
        if let float = Double(self){
            return CGFloat(float)
        }
        return 0
    }
    
    ///转换为Bool类型
    var boolValue : Bool {
        let tempStr = NSString(string: self)
        return tempStr.boolValue
    }
}


public extension String {
 
    //获取链接参数
    func parametersFromUrl() -> [String:String]? {
        guard let url = URL.init(string: self) else {
            return nil
        }
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
              let queryItems = components.queryItems else {
                return nil
        }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
    
    //手机号验证
    func isPhoneValid() -> Bool{
        let pattern = "^1[0-9]{10}$"
        let regex = try! NSRegularExpression.init(pattern: pattern, options: NSRegularExpression.Options(rawValue:0))
        let res = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue:0), range: NSMakeRange(0, self.count))
        if res.count > 0 {
            return true
        }
        return false
    }
    // 判断输入的字符串是否为数字，不含其它字符
    func isPureNumber() -> Bool {
        let scan: Scanner = Scanner(string: self)
        var val:Int = 0
        return scan.scanInt(&val) && scan.isAtEnd
    }

    //邮箱验证
    func isEmailValid() -> Bool{
        let pattern = "^[A-Za-z\\d]+([-_.][A-Za-z\\d]+)*@([A-Za-z\\d]+[-.])+[A-Za-z\\d]{2,4}$"
        let regex = try! NSRegularExpression.init(pattern: pattern, options: NSRegularExpression.Options(rawValue:0))
        let res = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue:0), range: NSMakeRange(0, self.count))
        if res.count > 0 {
            return true
        }
        return false
    }
    
    //姓名验证
    func isNameValid() -> Bool{
        let pattern = "^[\\u4E00-\\u9FA5\\uf900-\\ufa2d·s]{2,20}$"
        let regex = try! NSRegularExpression.init(pattern: pattern, options: NSRegularExpression.Options(rawValue:0))
        let res = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue:0), range: NSMakeRange(0, self.count))
        if res.count > 0 {
            return true
        }
        return false
    }
    
    //用户名裁剪
    func nameEncrypt()-> String{
        if self.count > 0{
            var mobile = self
            mobile.replaceSubrange(mobile.index(mobile.startIndex, offsetBy: 1)..<mobile.index(mobile.startIndex, offsetBy: self.count), with: "**")
            return mobile
        }
        return self
    }
    
    //手机号加密
    func phoneNumberEncrypt()-> String{
        if self.count == 11{
            var mobile = self
            mobile.replaceSubrange(mobile.index(mobile.startIndex, offsetBy: 3)..<mobile.index(mobile.startIndex, offsetBy: 7), with: "****")
            return mobile
        }
        return self
    }
     
    func getStrHeight(width:CGFloat, font:UIFont, space:CGFloat = 10)-> CGFloat {
        
        let size = self.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil).size
        return size.height + space
    }
    
    func getStrWidth(height:CGFloat, font:UIFont, space:CGFloat = 10)-> CGFloat {
        
        let size = self.boundingRect(with: CGSize(width: kSCREEN_WIDTH, height: height),
                                     options: [.usesLineFragmentOrigin, .usesFontLeading],
                                     attributes: [NSAttributedString.Key.font: font], context: nil).size
        return size.width + space
    }
    
    //    MARK: - 字符串转日期
    func dateFromString() ->NSDate {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let date = dateFormatter.date(from: self)
        
        return date! as NSDate
    }
    
    static func dateStrFrom(timeInterval:Int) -> String {
        
        let date = NSDate.init(timeIntervalSince1970: TimeInterval(timeInterval))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM月dd日"
        let dateStr = dateFormatter.string(from: date as Date)
        return dateStr
    }
    
    static func dateStrFromNow( offset:Int = 0) -> String {
        
        let now = NSDate().timeIntervalSince1970
        let time = now - TimeInterval(offset)
        let date = NSDate.init(timeIntervalSince1970: time)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateStr = dateFormatter.string(from: date as Date)

        return dateStr
    }

    /*
     时间转化为时间戳
     :param: stringTime 时间为 String
     :returns: 返回 时间戳为Int 秒为单位
     */
    static func stringToTimeStamp(stringTime:String) -> Int64 {
        
        let dfmatter = DateFormatter()
        if stringTime.count == 19 {
            // 返回数据中 没有毫秒
            dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
        }else if stringTime.count == 23 {
            // 返回数据中 有毫秒
            dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss.SSS"
        }else if stringTime.count == 10 {
            dfmatter.dateFormat="yyyy-MM-dd"
        }
        if let date = dfmatter.date(from: stringTime){
            let dateStamp = date.timeIntervalSince1970
            
            let time:Int64 = Int64(dateStamp)
            return time
        }
        return 0
    }
    
    /*
     时间戳转换时间
     :param: dateStr 时间戳字符串
     :returns: 需要的时间字符串
     */
    static func dateStrFromNum(time:Int, type:Int = 0) -> String {
        
        let date:Date = Date(timeIntervalSince1970: Double(time))
        
        let df = DateFormatter()
        if type == 0{
            df.dateFormat = "yyyy-MM-dd HH:mm"
        }else{
            df.dateFormat = "MM月dd日 HH:mm"
        }
//        if isDate{
//            df.dateFormat = "yyyy-MM-dd"
//        }else{
//            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        }
        return df .string(from: date)
        
    }
    
    static func getTimeStr(time: Int) -> String {
        
        let nowTime = Date().timeIntervalSince1970
        let timeZone = (Int(nowTime) - time)
        var interval:Int =  Int(timeZone)
        interval = interval>0 ? interval : 0
        
        var hour = 0
        var minutes = 0
        var day = 0
        
        let daytime = 24 * 60 * 60
        if interval >= daytime{
            day = interval/daytime
            hour = (interval % daytime) / 3600
            minutes = ((interval % daytime) % 3600) / 60
        }else if interval >= 60 * 60 {
            hour = interval/3600
            minutes = (interval%3600)/60
        }else{
            minutes = interval / 60
        }
        
        var timeStr = ""
        if day != 0 {
            timeStr += String(format:"%d天", day)
        }
        if hour != 0 {
            timeStr += String(format:"%d小时", hour)
        }
        if minutes != 0 {
            timeStr += String(format:"%d分钟", minutes)
        }
        if timeStr.count == 0 {
            timeStr += "0分钟"
        }
        return timeStr + "前"
    }

}

/// 随机字符串生成
public extension String{
    
    static let allCharacters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    static func randomStr(len : Int) -> String{
        var ranStr = ""
        for _ in 0..<len {
            let index = Int(arc4random_uniform(UInt32(allCharacters.count)))
            ranStr.append(allCharacters[allCharacters.index(allCharacters.startIndex, offsetBy: index)])
        }
        return ranStr
    }
    
    
    func unicodeFormat() -> String{
        if self.contains("\\u") == false && self.contains("\\U") == false{
            return self
        }
        
        //转码
        var str : String = self
        let temp1 = self.replacingOccurrences(of: "\\u", with: "\\U")
        let temp2 = temp1.replacingOccurrences(of: "\"", with: "\\\"")
        let temp3 = "\"".appending(temp2).appending("\"")
        if let data = temp3.data(using: String.Encoding.utf8), data.count > 0{
            do {
                if let s = try PropertyListSerialization.propertyList(from: data, options: [.mutableContainers], format: nil) as? String{
                    str = s
                    str = str.replacingOccurrences(of: "\\r\\n", with: "\n")
                }
            } catch {
                //MYLog("字符串 Unicode 转码失败, error = \(error.localizedDescription)")
            }
        }
        return str
    }
    
    func nk_urlFormat() ->String{
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? self
    }

}
extension String {
    var asAttributedString: NSAttributedString? {
        guard let data = self.data(using: .utf8) else { return nil }
        return try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
    }
}


extension String {
    var trimmed: String {
        self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    mutating func trim() {
        self = self.trimmed
    }
}

extension String {
    var asURL: URL? {
        URL(string: self)
    }
}

extension String {
    var containsOnlyDigits: Bool {
        let notDigits = NSCharacterSet.decimalDigits.inverted
        return rangeOfCharacter(from: notDigits, options: String.CompareOptions.literal, range: nil) == nil
    }
}

 
extension String {
    var asDict: [String: Any]? {
        guard let data = self.data(using: .utf8) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
    }
    
    var asArray: [Any]? {
        guard let data = self.data(using: .utf8) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Any]
    }

}

