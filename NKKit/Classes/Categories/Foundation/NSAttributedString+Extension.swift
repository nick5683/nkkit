//
//  NSAttributedString+Extension.swift
//
//  Created by Nick on 2019/11/21.
//

import UIKit

public func nk_att(_ string: String) -> NSAttributedString {
    return NSAttributedString(string: string)
}

public func nk_att_font(_ string: String, _ font: UIFont) -> NSAttributedString {
    return NSAttributedString(string: string, attributes: [.font: font])
}

public func nk_att_color(_ string: String, _ color: UIColor) -> NSAttributedString {
    return NSAttributedString(string: string, attributes: [.foregroundColor: color])
}

public func nk_att_fontColor(_ string: String, _ font: UIFont, _ color: UIColor) -> NSAttributedString {
    return NSAttributedString(string: string, attributes: [.font: font,
                                                           .foregroundColor: color])
}

public extension NKFramework where Base: NSAttributedString {
    /// 设置字符串行高，对齐方式
    /// - Parameters:
    ///   - spacing: 行高
    ///   - textAlignment: 对齐方式
    func setLineSpacing(_ spacing: CGFloat, textAlignment: NSTextAlignment = .left) -> NSMutableAttributedString {
        let stringM = base.mutableCopy() as! NSMutableAttributedString
        let style = NSMutableParagraphStyle()
        style.lineSpacing = spacing
        style.alignment = textAlignment
        stringM.addAttribute(.paragraphStyle, value: style, range: NSMakeRange(0, base.string.count))
        return stringM
    }

    /// 在字符串尾部添加富文本，支持多个富文本
    func append(_ strings: [NSAttributedString]) -> NSMutableAttributedString {
        let stringM = base.mutableCopy() as! NSMutableAttributedString
        strings.forEach { stringM.append($0) }
        return stringM
    }

    /// 在字符串尾部添加富文本
    func append(_ string: NSAttributedString) -> NSMutableAttributedString {
        return append([string])
    }
}

public extension NKFramework where Base: NSAttributedString {
    
    func replacingOccurrences(of oldStringPart: String, with newStringPart: String) -> NSMutableAttributedString {
        guard let mutableAttributedString = base.mutableCopy() as? NSMutableAttributedString else {
            return base as! NSMutableAttributedString
        }
        let mutableString = mutableAttributedString.mutableString

        while mutableString.contains(oldStringPart) {
            let rangeOfStringToBeReplaced = mutableString.range(of: oldStringPart)
            mutableAttributedString.replaceCharacters(in: rangeOfStringToBeReplaced, with: newStringPart)
        }
        return mutableAttributedString
    }
    
    // MARK: 修改字体颜色
    func changeStringColor(_ allText: String, length: NSInteger, atIndex: NSInteger, color: UIColor)->NSMutableAttributedString{
        let dosageStr: NSMutableAttributedString = NSMutableAttributedString(string: allText)
        dosageStr.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(atIndex, length))
        return dosageStr
    }
    
    // MARK: 修改字体大小
    func changeStringFont(_ allText: NSMutableAttributedString, length: NSInteger, atIndex: NSInteger, font: UIFont)->NSMutableAttributedString{
        let dosageStr: NSMutableAttributedString = NSMutableAttributedString(attributedString: allText)
        dosageStr.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(atIndex, length))
        return dosageStr
    }
    
    // MARK: 修改文字行间距
    func changeLineSpacing(_ attributedString: NSMutableAttributedString) -> NSMutableAttributedString{
        var changedAttributedString: NSMutableAttributedString!
        if attributedString.length > 0 {
            changedAttributedString = NSMutableAttributedString(attributedString: attributedString)
            let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5
            changedAttributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        }
        return changedAttributedString
    }
}
