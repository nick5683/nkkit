//
//  Bundle+Info.swift
//  NKKit
//
//  Created by dhcc on 2024/1/29.
//

import UIKit

extension Bundle {
    var appVersion: String? {
        self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    static var mainAppVersion: String? {
        Bundle.main.appVersion
    }
    
    /* App名称 */
    var appName: String? {
        self.infoDictionary?["CFBundleDisplayName"] as? String
    }
    
    static var mainAppName: String? {
        Bundle.main.appName
    }

    var kAppBuildVersion: String? {
        self.infoDictionary?["CFBundleVersion"] as? String
    }
    
    static var appBuildVersion: String? {
        Bundle.main.kAppBuildVersion
    }

    var kAppBundleId: String? {
        self.infoDictionary?["CFBundleIdentifier"] as? String
    }
    
    static var mainBundleId: String? {
        Bundle.main.kAppBundleId
    }
    

}
 
