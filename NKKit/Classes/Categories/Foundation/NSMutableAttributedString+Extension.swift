//
//  NSMutableAttributedString+Extension.swift
//  Demo
//
//  Created by Nick on 2020/6/10.
//  Copyright © 2020 Nick. All rights reserved.
//

import UIKit


public extension NKFramework where Base: NSMutableAttributedString {
    func set(color: UIColor) -> NSMutableAttributedString {
        base.addAttribute(.foregroundColor, value: color, range: NSRange(location: 0, length: base.string.count))
        return base
    }

    func set(font: UIFont) -> NSMutableAttributedString {
        base.addAttribute(.font, value: font, range: NSRange(location: 0, length: base.string.count))
        return base
    }
}
