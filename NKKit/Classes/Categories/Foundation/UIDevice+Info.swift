//
//  UIDevice+Extension.swift
//  NKKit
//
//  Created by dhcc on 2023/12/23.
//

import UIKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony

public extension UIDevice {
    
    func MBiteFormatter(_ bytes: Int64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = ByteCountFormatter.Units.useMB
        formatter.countStyle = ByteCountFormatter.CountStyle.decimal
        formatter.includesUnit = false
        return formatter.string(fromByteCount: bytes) as String
    }
    
    
    var totalDiskSpaceInMB:String {
        return MBiteFormatter(totalDiskSpaceInBytes)
    }
    
    var freeDiskSpaceInMB:String {
        return MBiteFormatter(freeDiskSpaceInBytes)
    }
    
    var usedDiskSpaceInMB:String {
        return MBiteFormatter(usedDiskSpaceInBytes)
    }
    //MARK: Get String Value
    var totalDiskSpaceInGB:String {
       return ByteCountFormatter.string(fromByteCount: totalDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var freeDiskSpaceInGB:String {
        return ByteCountFormatter.string(fromByteCount: freeDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    var usedDiskSpaceInGB:String {
        return ByteCountFormatter.string(fromByteCount: usedDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }
    
    
    //MARK: Get raw value
    var totalDiskSpaceInBytes:Int64 {
        guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value else { return 0 }
        return space
    }
    
    /*
     Total available capacity in bytes for "Important" resources, including space expected to be cleared by purging non-essential and cached resources. "Important" means something that the user or application clearly expects to be present on the local system, but is ultimately replaceable. This would include items that the user has explicitly requested via the UI, and resources that an application requires in order to provide functionality.
     Examples: A video that the user has explicitly requested to watch but has not yet finished watching or an audio file that the user has requested to download.
     This value should not be used in determining if there is room for an irreplaceable resource. In the case of irreplaceable resources, always attempt to save the resource regardless of available capacity and handle failure as gracefully as possible.
     */
    var freeDiskSpaceInBytes:Int64 {
        if #available(iOS 11.0, *) {
            if let space = try? URL(fileURLWithPath: NSHomeDirectory() as String).resourceValues(forKeys: [URLResourceKey.volumeAvailableCapacityForImportantUsageKey]).volumeAvailableCapacityForImportantUsage {
                return space
            } else {
                return 0
            }
        } else {
            if let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value {
                return freeSpace
            } else {
                return 0
            }
        }
    }
    
    var usedDiskSpaceInBytes:Int64 {
       return totalDiskSpaceInBytes - freeDiskSpaceInBytes
    }
    
    
    /// 获取内存使用情况
    /// - Returns:
    ///     - used: 已使用
    ///     - total: 总内存
    static func memoryUsage() -> (used: UInt64, total: UInt64) {
        var taskInfo = task_vm_info_data_t()
        var count = mach_msg_type_number_t(MemoryLayout<task_vm_info>.size) / 4
        let result: kern_return_t = withUnsafeMutablePointer(to: &taskInfo) {
            $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
                task_info(mach_task_self_, task_flavor_t(TASK_VM_INFO), $0, &count)
            }
        }

        var used: UInt64 = 0
        if result == KERN_SUCCESS {
            used = UInt64(taskInfo.phys_footprint)
        }

        let total = ProcessInfo.processInfo.physicalMemory
        return (used, total)
    }
    
}
 
public extension UIDevice{
    //    判断是否是调试模式
    var isBeingDebugged: Bool {
        // Initialize all the fields so that,
        // if sysctl fails for some bizarre reason, we get a predictable result.
        var info = kinfo_proc()
        // Initialize mib, which tells sysctl the info we want,
        // in this case we're looking for info about a specific process ID.
        var mib = [CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid()]
        // Call sysctl.
        var size = MemoryLayout.stride(ofValue: info)
        let junk = sysctl(&mib, u_int(mib.count), &info, &size, nil, 0)
        assert(junk == 0, "sysctl failed")
        // We're being debugged if the P_TRACED flag is set.
        return (info.kp_proc.p_flag & P_TRACED) != 0
    }
    
    // 判断app是否运行在模拟器上
    func isSimulator() -> Bool {
#if targetEnvironment(simulator)
        return true
#else
        return false
#endif
    }
    // 判断是否设置了代理
    func isUsedProxy() -> Bool {
        guard let proxy = CFNetworkCopySystemProxySettings()?.takeUnretainedValue() else { return false }
        guard let dict = proxy as? [String: Any] else { return false }
//        let isUsed = dict.isEmpty // 有时候未设置代理dictionary也不为空，而是一个空字典
        guard let HTTPProxy = dict["HTTPProxy"] as? String else { return false }
        //        let a:String = HTTPProxy as! String
        if (HTTPProxy.count>0) {
            return true;
        }
        return false;
    }

    func isVPNConnected() -> Bool {
        guard let cfDict = CFNetworkCopySystemProxySettings() else{
            return false
        }
        let nsDict = cfDict.takeRetainedValue() as NSDictionary
        guard let keys = nsDict["__SCOPED__"] as? NSDictionary, let allKeys = keys.allKeys as? [String]  else{
            return false
        }
        for key: String in allKeys {
            if (key == "tap" || key == "tun" || key == "ppp" || key == "ipsec" || key == "ipsec0" || key == "utun1" || key == "utun2") {
                return true
            }
        }
        return false
    }
    /// 获取系统当前语言
    func getCurrentLanguage() -> String {
        // 返回设备曾使用过的语言列表
        if let languages: [String] = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String], let currentLanguage = languages.first {
            // 当前使用的语言排在第一
            return currentLanguage
        }
        return "en-CN"
    }
    
    /// 设备运营商IP（联通/移动/电信的运营商给的移动IP）
    var deviceIP:String{
        var addresses = [String]()
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while (ptr != nil) {
                let flags = Int32(ptr!.pointee.ifa_flags)
                var addr = ptr!.pointee.ifa_addr.pointee
                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                    if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                            if let address = String(validatingUTF8:hostname) {
                                addresses.append(address)
                            }
                        }
                    }
                }
                ptr = ptr!.pointee.ifa_next
            }
            freeifaddrs(ifaddr)
        }
        return addresses.first ?? ""
    }
    
}

public extension UIDevice{
    
    //获取设备当前连接的WiFi信息
    func currentWifiInfo() -> NSDictionary? {
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    return interfaceInfo
                }
            }
        }
        /**
         {
             BSSID = "44:59:43:7d:6b:26";
             SSID = "CE-8B44";
             SSIDDATA = {length = 7, bytes = 0x43552d36423734};
         }
         **/
       return nil
    }

    /// 获取手机SIM卡网络运营商名称
    func currentCarrierName() -> String? {
        var tempName: String?
        let info = CTTelephonyNetworkInfo()
        if #available(iOS 12.0, *) {
            if let carrierProviders = info.serviceSubscriberCellularProviders {
                for item in carrierProviders.values {
                    if item.mobileNetworkCode != nil {
                        tempName = item.carrierName
                    }
                }
            }
        } else {
            if let carrier: CTCarrier = info.subscriberCellularProvider {
                tempName = carrier.carrierName
                /*
                 print("数据业务信息：\(currentRadioTech)")
                 print("网络制式：\(getNetworkType(currentRadioTech: currentRadioTech))")
                 print("运营商名字：\(carrier.carrierName!)")
                 print("移动国家码(MCC)：\(carrier.mobileCountryCode!)")
                 print("移动网络码(MNC)：\(carrier.mobileNetworkCode!)")
                 print("ISO国家代码：\(carrier.isoCountryCode!)")
                 print("是否允许VoIP：\(carrier.allowsVOIP)"
                 
                 */
            }
        }
        return tempName
    }}

public extension UIDevice{
    
    var isJailbroken: Bool {
        return jailbreakFileExists
            || sandboxBreached
            || evidenceOfSymbolLinking
    }

    private var evidenceOfSymbolLinking: Bool {
        var s = stat()
        guard lstat("/Applications", &s) == 0 else { return false }
        return (s.st_mode & S_IFLNK == S_IFLNK)
    }

    private var sandboxBreached: Bool {
        guard (try? " ".write(
            toFile: "/private/jailbreak.txt",
            atomically: true, encoding: .utf8)) == nil else {
                return true
        }
        return false
    }

    private var jailbreakFileExists: Bool {
        let jailbreakFilePaths = [
            "/Applications/Cydia.app",
            "/Library/MobileSubstrate/MobileSubstrate.dylib",
            "/bin/bash",
            "/usr/sbin/sshd",
            "/etc/apt",
            "/private/var/lib/apt/"
        ]
        let fileManager = FileManager.default
        return jailbreakFilePaths.contains { path in
            if fileManager.fileExists(atPath: path) {
                return true
            }
            if let file = fopen(path, "r") {
                fclose(file)
                return true
            }
            return false
        }
    }




}

public extension UIDevice {
    //信号强度
    func getSignalStrength() -> Int {
        let application = UIApplication.shared
        guard let statusBarView = application.value(forKey: "statusBar") as? UIView,
              let foregroundView = statusBarView.value(forKey: "foregroundView") as? UIView else{
            return 0
        }
        let foregroundViewSubviews = foregroundView.subviews

        var dataNetworkItemView:UIView?

        let cls: AnyClass? = NSClassFromString("UIStatusBarSignalStrengthItemView")

        for subview in foregroundViewSubviews {
            if let tmpCls = cls{
                if ( subview.isKind(of: tmpCls)) {
                    dataNetworkItemView = subview
                    print("NONE")
                    break
                }
            }else{
                print("NONE")
                return 0 //NO SERVICE
            }
                
        }

        return dataNetworkItemView?.value(forKey: "signalStrengthBars") as? Int ?? 0
    }

}
 
