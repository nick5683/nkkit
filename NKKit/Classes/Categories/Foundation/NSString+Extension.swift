//
//  NSString+Extension.swift
//
//  Created by Nick on 2019/11/21.
//

import UIKit

public extension NKFramework where Base: NSString {
    /// 获取字符串布局范围
    /// - Parameters:
    ///   - size: 布局大小
    ///   - attributes: 文字样式
    func boundingRect(with size: CGSize, attributes: Dictionary<NSAttributedString.Key, Any>) -> CGRect {
        return base.boundingRect(with: size, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: attributes, context: nil)
    }
    
    /// 获取字符串高度
    /// - Parameters:
    ///   - width: 布局宽度
    ///   - attributes: 文字样式
    func height(with width: CGFloat, attributes:  Dictionary<NSAttributedString.Key, Any>) -> CGFloat {
        return boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), attributes: attributes).size.height
    }
    
    // MARK: - 富文本
    
    /// 设置字符串行高，对齐方式
    /// - Parameters:
    ///   - spacing: 行高
    ///   - textAlignment: 对齐方式
    func setLineSpacing(_ spacing: CGFloat, textAlignment: NSTextAlignment = .left) -> NSMutableAttributedString {
        return NSAttributedString(string: base as String).nk.setLineSpacing(spacing, textAlignment: textAlignment)
    }
    
    /// 在字符串尾部添加富文本，单个富文本
    /// - Parameter string: NSAttributedString
    func append(_ string: NSAttributedString) -> NSMutableAttributedString {
        return append([string])
    }
    
    /// 在字符串尾部添加富文本，支持多个富文本
    /// - Parameter strings: [NSAttributedString]
    func append(_ strings: [NSAttributedString]) -> NSMutableAttributedString {
        return NSAttributedString(string: base as String).nk.append(strings)
    }
    
    func attributed() -> NSMutableAttributedString {
        return NSMutableAttributedString(string: base as String)
    }
}



public extension String {
    
    //Base64编码
    func nk_encodBase64() -> String {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return ""
    }
    //Base64解码
    func nk_decodeBase64() -> String?{
        if let data = Data(base64Encoded: self) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    
    func nk_FormatterMoney(number:CGFloat) -> String{
        //    truncatingRemainder 进行浮点数取余
        //    任何数字余1 都为小数部分
        return number.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", number) : String(format: "%.2f", number)
    }

}
