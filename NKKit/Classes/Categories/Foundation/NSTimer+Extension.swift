//
//  NSTimer+Extension.swift
//
//  Created by Nick on 2019/11/21.
//

import Foundation

public extension Timer {
    static func nk_timer(with timeInterval: TimeInterval, block: @escaping () -> Void) -> Timer {
        return Timer(timeInterval: timeInterval, target: self, selector: #selector(nk_timerAction(_:)), userInfo: block, repeats: true)
    }

    @objc private static func nk_timerAction(_ timer: Timer) {
        let block = timer.userInfo as? () -> Void
        block?()
    }
}
