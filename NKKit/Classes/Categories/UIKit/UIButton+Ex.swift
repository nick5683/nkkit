//
//  NK_UIButtonExtension.swift
//  NKKit
//
//  Created by dhcc on 2023/11/30.
//

import UIKit

public extension UIButton {
    
    //MARK: -定义button相对label的位置
    enum NKButtonEdgeInsetsStyle {
        case Top
        case Left
        case Right
        case Bottom
    }

    //replaceStrToFunc
    func layoutButton(style: NKButtonEdgeInsetsStyle, imageTitleSpace: CGFloat, padding:CGFloat=5) {
        //得到imageView和titleLabel的宽高
        let imageWidth = self.imageView?.frame.size.width
        let imageHeight = self.imageView?.frame.size.height
        
        var labelWidth: CGFloat! = 0.0
        var labelHeight: CGFloat! = 0.0

        let version = (UIDevice.current.systemVersion as NSString).doubleValue
        if CGFloat(version) >= 8.0 {
            labelWidth = self.titleLabel?.intrinsicContentSize.width
            labelHeight = self.titleLabel?.intrinsicContentSize.height
        }else{
            labelWidth = self.titleLabel?.frame.size.width
            labelHeight = self.titleLabel?.frame.size.height
        }
        
        //初始化imageEdgeInsets和labelEdgeInsets
        var imageEdgeInsets = UIEdgeInsets.zero
        var labelEdgeInsets = UIEdgeInsets.zero
        
        //根据style和space得到imageEdgeInsets和labelEdgeInsets的值
        switch style {
        case .Top:
            //上 左 下 右
            imageEdgeInsets = UIEdgeInsets(top: -labelHeight-imageTitleSpace/2, left: 0, bottom: 0, right: -labelWidth)
            labelEdgeInsets = UIEdgeInsets(top: 0, left: -imageWidth!, bottom: -imageHeight!-imageTitleSpace/2, right: 0)
            break;
            
        case .Left:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -imageTitleSpace/2, bottom: 0, right: imageTitleSpace)
            labelEdgeInsets = UIEdgeInsets(top: 0, left: imageTitleSpace/2, bottom: 0, right: -imageTitleSpace/2)
            break;
            
        case .Bottom:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: -labelHeight!-imageTitleSpace/2, right: -labelWidth)
            labelEdgeInsets = UIEdgeInsets(top: -imageHeight!-imageTitleSpace/2, left: -imageWidth!, bottom: 0, right: 0)
            break;
            
        case .Right:
            
            imageEdgeInsets = UIEdgeInsets(top: padding, left: labelWidth+imageTitleSpace/2, bottom: padding, right: -labelWidth-imageTitleSpace/2)
            labelEdgeInsets = UIEdgeInsets(top: padding, left: -imageWidth!-imageTitleSpace/2, bottom: padding, right: imageWidth!+imageTitleSpace/2)

            break;
            
        }
        
        self.titleEdgeInsets = labelEdgeInsets
        self.imageEdgeInsets = imageEdgeInsets
        
    }
}


public extension UIButton {
    
    func setTitleStr(_ title:String){
        
        setTitle(title, for: .normal)
        setTitle(title, for: .highlighted)
    }
    
    func nk_CreateBtn(title:String?=nil, titleColor:UIColor?=nil,
                             image:UIImage?=nil, bgImage:UIImage?=nil,
                             corRadius:CGFloat=0, bgColor:UIColor?=nil)  -> UIButton{
        let nk_btn = UIButton()
        if (corRadius>0){
            nk_btn.layer.cornerRadius = corRadius
            nk_btn.layer.masksToBounds = true
        }
        if let str = title{
            nk_btn.setTitle(str, for: .normal)
            nk_btn.setTitle(str, for: .highlighted)
        }
        if let img = image{
            nk_btn.setImage(img, for: .normal)
            nk_btn.setImage(img, for: .highlighted)
        }
        
        if let bgImg = bgImage{
            nk_btn.setBackgroundImage(bgImg, for: .normal)
            nk_btn.setBackgroundImage(bgImg, for: .highlighted)
            
        }
        
        nk_btn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        nk_btn.setTitleColor(titleColor ?? UIColor.white, for: .normal)
        nk_btn.backgroundColor = bgColor ?? UIColor.white
        return nk_btn
        
    }
}

