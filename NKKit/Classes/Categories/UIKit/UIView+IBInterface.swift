//
//  UIView+IBInterface.swift
//
//  Created by Nick on 2019/11/13.
//

import UIKit
import QuartzCore

public extension UIView {
    /// 边框颜色
    @IBInspectable var nk_borderColor: UIColor? {
        set { layer.borderColor = newValue?.cgColor }
        get {  return layer.borderColor == nil ? nil : UIColor(cgColor: layer.borderColor!) }
    }
    
    /// 边框宽度
    @IBInspectable var nk_borderWidth: CGFloat {
        set { layer.borderWidth = newValue }
        get { return layer.borderWidth }
    }
    
    /// 圆角大小
    @IBInspectable var nk_cornerRadius: CGFloat {
        set { layer.cornerRadius = newValue }
        get { return layer.cornerRadius }
    }
    
    @IBInspectable var nk_shadowRadius: CGFloat {
        set { layer.shadowRadius = newValue }
        get { return layer.shadowRadius }
    }
}


//frame 相关
extension UIView{
    /// x值
    var x : CGFloat{
        set{
            self.frame.origin.x = newValue
        }
        get{
            return self.frame.origin.x
        }
    }
    /// minX值
    var minX : CGFloat{
        set{
            self.frame.origin.x = newValue
        }
        get{
            return self.frame.minX
        }
    }
    var left : CGFloat{
        set{
            self.frame.origin.x = newValue
        }
        get{
            return self.frame.origin.x
        }
    }
    
    /// mid值
    var midX : CGFloat{
        set{
            self.frame.origin.x = newValue-self.frame.size.width/2.0
        }
        get{
            return self.frame.midX
        }
    }
    
    /// maxX值
    var maxX : CGFloat{
        get{
            return self.frame.maxX
        }
    }
    var right : CGFloat{
        set{
            self.frame.origin.x = newValue-self.frame.size.width
        }
        get{
            return self.frame.maxX
        }
    }
    
    //-----------
    /// y值
    var y : CGFloat{
        get{
            return self.frame.origin.y
        }
        set{
            self.frame.origin.y = newValue
        }
    }
    /// minY值
    var minY : CGFloat{
        get{
            return self.frame.minY
        }
        set{
            self.frame.origin.y = newValue
        }
    }
    var top : CGFloat{
        get{
            return self.frame.origin.y
        }
        set{
            self.frame.origin.y = newValue
        }
    }
    
    /// maxY值
    var midY : CGFloat{
        set{
            self.frame.origin.y = newValue - self.frame.height/2.0
        }
        get{
            return self.frame.midY
        }
    }
    
    /// maxY值
    var maxY : CGFloat{
        get{
            return self.frame.maxY
        }
        set{
            self.frame.origin.y = newValue - self.frame.height
        }
    }
    var bottom : CGFloat{
        get{
            return self.frame.maxY
        }
        set{
            self.frame.origin.y = newValue - self.frame.height
        }
    }

    //---------------
    /// width值
    var width : CGFloat{
        get{
            return self.frame.size.width
        }
        set{
            self.frame.size.width = newValue
        }
    }
    
    /// height值
    var height : CGFloat{
        get{
            return self.frame.size.height
        }
        set{
            self.frame.size.height = newValue
        }
    }
    
    /// 中心点x值
    var centerX : CGFloat{
        get{
            return self.center.x
        }
        set{
            self.center.x = newValue
        }
    }
    
    /// 中心点y值
    var centerY : CGFloat{
        get{
            return self.center.y
        }
        set{
            self.center.y = newValue
        }
    }
    
    /// size值
    var origin : CGPoint{
        get{
            return self.frame.origin
        }
        set{
            self.frame.origin = newValue
        }
    }
    
    /// size值
    var size : CGSize{
        get{
            return self.frame.size
        }
        set{
            self.frame.size = newValue
        }
    }
    

    //所在的控制器
    public var ownedController : UIViewController?{
        var tempVC : UIViewController?
        
        //寻找当前view所在的控制器
        var tempRes : UIResponder? = self
        repeat{
            tempRes = tempRes?.next
        }while (tempRes != nil && !tempRes!.isKind(of: UIViewController.self))
        
        //是否找到
        if let res = tempRes{
            if let vc = res as? UIViewController{
                tempVC = vc
            }
        }
        return tempVC
    }
}

