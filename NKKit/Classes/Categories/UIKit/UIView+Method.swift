//
//  UIView+Method.swift
//  NKKit
//
//  Created by dhcc on 2023/12/23.
//

import UIKit
import QuartzCore
//
//
//func nk_loadNib<Q:UIView>(_: Q.Type, nibName: String) -> (Q){
//    let view = Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)?.first as! Q
//    return view
//}

public extension UIView {
    
    /**
        页面灰度设置。可以加载window上，也可以加到单独页面上
     */
    static func addGreyFilterToView(view:UIView) {
        let coverView = UIView.init(frame: UIScreen.main.bounds)
        coverView.isUserInteractionEnabled = false;
        coverView.tag = 1024;
        coverView.backgroundColor = UIColor.lightGray;
        coverView.layer.compositingFilter = "saturationBlendMode";
        coverView.layer.zPosition = CGFloat(MAXFLOAT);
        view.addSubview(coverView)
    }

    static func removeGreyFilterFromView(view:UIView) {
        if let greyView = view.viewWithTag(1024){
            greyView.removeFromSuperview();
        }
    }
    //replaceStrToFunc

}
public extension UIView {
    
    func addTransitionAnimation(duration:CFTimeInterval, type:CATransitionType, subtype:CATransitionSubtype, timingFunction:CAMediaTimingFunctionName, forKey:String?) {
        let animation = CATransition()
        animation.duration = duration
        animation.type = type
        animation.subtype = subtype
        animation.timingFunction = CAMediaTimingFunction(name: timingFunction)
        
        self.layer.add(animation, forKey: forKey)
    }

    /* 创建图片点赞动画 */
    func praise(point:CGPoint, view:UIView){
        let imageView = UIImageView()
        imageView.frame = CGRect(origin: point, size: CGSize(width: 35, height: 35))
        imageView.image = UIImage(named: "heart")
        imageView.backgroundColor = UIColor.clear
        imageView.clipsToBounds = true
        self.insertSubview(imageView, belowSubview: view)
        
        let startX = round(Double(arc4random() % 200))
        let scale = round(Double(arc4random() % 2)) + 1.0
        let speed = 1 / round(Double(arc4random() % 900)) + 0.6
        let imageName:Int = Int(round(Double(arc4random() % 12)))
        
        UIView.animate(withDuration: 7 * speed, animations: {
            imageView.image = UIImage(named: "resource.bundle/heart\(imageName)")
            imageView.frame = CGRect(x: kSCREEN_WIDTH - CGFloat(startX), y: -100, width: 35 * CGFloat(scale), height: 35 * CGFloat(scale))
        }) { (finish) in
            if finish{
                imageView.removeFromSuperview()
            }
        }
    }
    
    func addCorner(conrners: UIRectCorner , radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: conrners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
    }
    
   func addCorner(roundingCorners: UIRectCorner, cornerSize: CGSize) {
       //        for roundingCorner: UIRectCorner in roundingCorners {
       let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: roundingCorners, cornerRadii: cornerSize)
       let cornerLayer = CAShapeLayer()
       cornerLayer.frame = bounds
       cornerLayer.path = path.cgPath
       layer.mask = cornerLayer
       //        }
   }
   
}
 
//MARK: -----------  LOADING ---------------

public extension UIView {
    
    func addSubviews(_ views: UIView...){
        views.forEach { (view) in
            self.addSubview(view)
        }
    }
    
    func removeAllSubviews(){
        self.subviews.forEach { vi in
            vi.removeFromSuperview()
        }
    }

    func convertToImage() -> UIImage {
        
        let width = self.bounds.size.width != 0 ? self.bounds.size.width : 1
        let height = self.bounds.size.height != 0 ? self.bounds.size.height : 1
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, kSCREEN_SCALE)
        
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    
}

public extension UIView{
    
    func removeGradientLayer(){
        if let firstLayer = self.layer.sublayers?.first{
            if firstLayer is CAGradientLayer {
                firstLayer.removeFromSuperlayer()
            }
        }
    }
    func insertGradientLayer(beginPoint:CGPoint = CGPoint(x: 0, y: 0.5), endPoint:CGPoint = CGPoint(x:1, y: 0.5), colors:[CGColor], size: CGSize){
        
        if let firstLayer = self.layer.sublayers?.first{
            if firstLayer is CAGradientLayer {
                firstLayer.removeFromSuperlayer()
            }
        }
        let nk_gradient = CAGradientLayer()
        nk_gradient.frame = self.bounds
        nk_gradient.colors = colors;
        nk_gradient.startPoint = beginPoint
        nk_gradient.endPoint = endPoint
        self.layer.insertSublayer(nk_gradient, at: 0)
        
    }
    
}

public extension UIView{
    
    func addItemScaleAnimation(){
        let scaleAnimation = CAKeyframeAnimation()
        scaleAnimation.keyPath = "transform.scale"
        scaleAnimation.values = [1.0, 1.05, 1.20, 0.85, 1.0]
        scaleAnimation.duration = 0.5
        scaleAnimation.calculationMode = CAAnimationCalculationMode.cubic
        scaleAnimation.repeatCount = 1

        self.layer.add(scaleAnimation, forKey: "itemAnimation")
    }

}

//手势相关
public extension UIView : UIGestureRecognizerDelegate{
    
    //手势类型枚举
    enum GestureType {
        case tap       //点击
        case pan       //拖拽
        case swipe     //轻扫
        case longPress //长按
        case rotation  //旋转
        case pinch     //捏合
    }


    ///点按回调key
    fileprivate static var NK_GestureCallBackKey_tap = "NK_GestureCallBackKey_tap"
    ///拖拽回调key
    fileprivate static var NK_GestureCallBackKey_pan = "NK_GestureCallBackKey_pan"
    ///轻扫回调key
    fileprivate static var NK_GestureCallBackKey_swipe = "NK_GestureCallBackKey_swipe"
    ///长按回调key
    fileprivate static var NK_GestureCallBackKey_longPress = "NK_GestureCallBackKey_longPress"
    ///旋转回调key
    fileprivate static var NK_GestureCallBackKey_rotation = "NK_GestureCallBackKey_rotation"
    ///捏合回调key
    fileprivate static var NK_GestureCallBackKey_pinch = "NK_GestureCallBackKey_pinch"
    
    /// 添加手势
    /// - Parameter gestures: 手势类型数组,支持多种手势组合
    /// - Parameter callBack: 手势回调.(返回手势对象)
    func addGesture(_ gestures : [GestureType] , callBack:((UIGestureRecognizer)->Void)?){
        
        //无手势
        if gestures.isEmpty {
            print("手势类型为空, 请核查")
            return
        }
        
        //添加手势
        for type in gestures {
            switch type {
            case .tap:
                self.addTapGesture(callBack)
            case .pan:
                self.addPanGesture(callBack)
            case .swipe:
                self.addSwipeGesture(callBack)
            case .longPress:
                self.addLongPressGesture(callBack)
            case .rotation:
                self.addRotationGesture(callBack)
            case .pinch:
                self.addPinchGesture(callBack)
            }
        }
        
    }
    
    /// 添加点按手势
    /// - Parameter callBack: 回调
    func addTapGesture(_ callBack : ((UITapGestureRecognizer)->Void)?) {
        //有手势, 检测是否允许交互
        self.isUserInteractionEnabled = true
        
        //引用回调
        if let cb = callBack{
            objc_setAssociatedObject(self, &(UIView.NK_GestureCallBackKey_tap), cb, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        let ges = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        self.addGestureRecognizer(ges)
    }
    @objc fileprivate func tapAction(_ ges : UITapGestureRecognizer) {
        if let cb = objc_getAssociatedObject(self, &(UIView.NK_GestureCallBackKey_tap)) as? (UITapGestureRecognizer)->Void{
            cb(ges)
        }
    }
    
    /// 添加拖动手势
    /// - Parameter callBack: 回调
    func addPanGesture(_ callBack : ((UIPanGestureRecognizer)->Void)?) {
        //有手势, 检测是否允许交互
        self.isUserInteractionEnabled = true
        
        //引用回调
        if let cb = callBack{
            objc_setAssociatedObject(self, &(UIView.NK_GestureCallBackKey_pan), cb, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        let ges = UIPanGestureRecognizer(target: self, action: #selector(panAction(_:)))
        self.addGestureRecognizer(ges)
    }
    @objc fileprivate func panAction(_ ges : UIPanGestureRecognizer) {
        //获取偏移量,获取的偏移量是相对于最原始的点
        let transPoint = ges.translation(in: ges.view)
        
        //注意:不能使用带make的,带make的只一次性有效,下一次还是相对于原始点做位移
        let t = CGAffineTransform.translatedBy(self.transform)
        self.transform = t(transPoint.x, transPoint.y)
        
        //清0操作(不让偏移量进行累加,获取的是相对于上一次的值,每一次走的值.)
        ges.setTranslation(CGPoint.zero, in: ges.view)
        
        //回调
        if let cb = objc_getAssociatedObject(self, &(UIView.NK_GestureCallBackKey_pan)) as? (UIPanGestureRecognizer)->Void{
            cb(ges)
        }
    }
    
    /// 添加轻扫手势
    /// - Parameter callBack: 回调
    func addSwipeGesture(_ callBack : ((UISwipeGestureRecognizer)->Void)?) {
        //有手势, 检测是否允许交互
        self.isUserInteractionEnabled = true
        
        //引用回调
        if let cb = callBack{
            objc_setAssociatedObject(self, &(UIView.NK_GestureCallBackKey_swipe), cb, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        let ges = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        ges.direction = .left
        self.addGestureRecognizer(ges)
        
        let ges1 = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        ges1.direction = .right
        self.addGestureRecognizer(ges1)
        
        let ges2 = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        ges2.direction = .up
        self.addGestureRecognizer(ges2)
        
        let ges3 = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        ges3.direction = .down
        self.addGestureRecognizer(ges3)
    }
    @objc fileprivate func swipeAction(_ ges : UISwipeGestureRecognizer) {
        if let cb = objc_getAssociatedObject(self, &(UIView.NK_GestureCallBackKey_swipe)) as? (UISwipeGestureRecognizer)->Void{
            cb(ges)
        }
    }
    
    /// 添加长按手势
    /// - Parameter callBack: 回调
    func addLongPressGesture(_ callBack : ((UILongPressGestureRecognizer)->Void)?) {
        //有手势, 检测是否允许交互
        self.isUserInteractionEnabled = true
        
        //引用回调
        if let cb = callBack{
            objc_setAssociatedObject(self, &(UIView.NK_GestureCallBackKey_longPress), cb, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        let ges = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(_:)))
        self.addGestureRecognizer(ges)
    }
    @objc fileprivate func longPress(_ ges : UILongPressGestureRecognizer) {
        if let cb = objc_getAssociatedObject(self, &(UIView.NK_GestureCallBackKey_longPress)) as? (UILongPressGestureRecognizer)->Void{
            cb(ges)
        }
    }
    
    /// 添加旋转手势
    /// - Parameter callBack: 回调
    func addRotationGesture(_ callBack : ((UIRotationGestureRecognizer)->Void)?) {
        //有手势, 检测是否允许交互
        self.isUserInteractionEnabled = true
        
        //引用回调
        if let cb = callBack{
            objc_setAssociatedObject(self, &(UIView.NK_GestureCallBackKey_rotation), cb, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        let ges = UIRotationGestureRecognizer(target: self, action: #selector(self.rotationAction(_:)))
        ges.delegate = self
        self.addGestureRecognizer(ges)
    }
    @objc fileprivate func rotationAction(_ ges : UIRotationGestureRecognizer) {
        
        //获取旋转角度(已经是弧度),相对于最原始的弧度
        let rotatinon = ges.rotation
        let t = CGAffineTransform.rotated(ges.view!.transform)
        self.transform = t(rotatinon)
        //清0
        ges.rotation = 0
        
        if let cb = objc_getAssociatedObject(self, &(UIView.NK_GestureCallBackKey_rotation)) as? (UIRotationGestureRecognizer)->Void{
            cb(ges)
        }
    }
    
    /// 添加捏合手势
    /// - Parameter callBack: 回调
    func addPinchGesture(_ callBack : ((UIPinchGestureRecognizer)->Void)?) {
        //有手势, 检测是否允许交互
        self.isUserInteractionEnabled = true
        
        //引用回调
        if let cb = callBack{
            objc_setAssociatedObject(self, &(UIView.NK_GestureCallBackKey_pinch), cb, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        let ges = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchAction(_:)))
        ges.delegate = self
        self.addGestureRecognizer(ges)
    }
    @objc fileprivate func pinchAction(_ ges : UIPinchGestureRecognizer) {
        
        //获取缩放比例(相对于最原始的比例)
        let scale = ges.scale
        let t = CGAffineTransform.scaledBy(ges.view!.transform)
        self.transform = t(scale , scale)
        //重置
        ges.scale = 1.0
        
        if let cb = objc_getAssociatedObject(self, &(UIView.NK_GestureCallBackKey_pinch)) as? (UIPinchGestureRecognizer)->Void{
            cb(ges)
        }
    }
    
    //是否支持多手势
//    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        //有多个手势时, 支持多手势
//        if let gesArr = self.gestureRecognizers{
//            return gesArr.count > 1 ? true : false
//        }
//        return false
//    }
    
}
