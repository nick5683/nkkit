//
//  UIViewController+DWAlert.swift
//
//  Created by Nick on 2019/12/10.
//

import UIKit

public extension NKFramework where Base: UIViewController {
    
    /// 显示确认提示
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 提示
    ///   - confirmTitle: 确认按钮标题
    ///   - block: 回调
    func showConfirmAlert(title: String?, message: String?, confirmTitle: String, block: (() -> Void)?) {
        showAlert(title: title, message: message, style: .alert, actions: ["取消", confirmTitle], cancelIndex: 0, destructiveIndex: 1) { index in
            if index == 1 { block?() }
        }
    }

    /// 显示系统弹窗
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 提示
    ///   - actions: 操作按钮
    ///   - block: 回调
    func showAlert(title: String?, message: String?, actions: [String], block: ((Int) -> Void)?) {
        showAlert(title: title, message: message, style: .alert, actions: actions, block: block)
    }

    /// 显示系统弹窗
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 提示
    ///   - style: 样式
    ///   - actions: 操作按钮
    ///   - block: 回调
    func showAlert(title: String?, message: String?, style: UIAlertController.Style, actions: [String], block: ((Int) -> Void)?) {
        showAlert(title: title, message: message, style: style, actions: actions, cancelIndex: -1, destructiveIndex: -1, block: block)
    }

    /// 显示系统弹窗
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 提示
    ///   - style: 样式
    ///   - actions: 操作按钮
    ///   - cancelIndex: 取消按钮index
    ///   - destructiveIndex: 红字按钮index
    ///   - block: 回调
    func showAlert(title: String?, message: String?, style: UIAlertController.Style, actions: [String], cancelIndex: Int, destructiveIndex: Int, block: ((Int) -> Void)?) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: style)
        for i in 0 ..< actions.count {
            let actionStyle: UIAlertAction.Style = cancelIndex == i ? .cancel : (destructiveIndex == i ? .destructive : .default)
            let action = UIAlertAction(title: actions[i], style: actionStyle) { _ in
                block?(i)
            }
            alertVC.addAction(action)
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            if let popPresenter = alertVC.popoverPresentationController {
                popPresenter.sourceView = base.view // 这就是挂靠的对象
                popPresenter.sourceRect = base.view.bounds
                popPresenter.permittedArrowDirections = UIPopoverArrowDirection.any;
            }
        }


        DispatchQueue.main.async {
            self.base.present(alertVC, animated: true, completion: nil)
        }
    }
}
