//
//  UIColor+Extension.swift
//
//  Created by Nick on 2019/11/20.
//

import UIKit


public extension UIColor {
    
    /// 16进制颜色
    convenience init(hex: Int, alpha: CGFloat = 1) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255
        let green = CGFloat((hex & 0xFF00) >> 8) / 255
        let blue = CGFloat(hex & 0xFF) / 255
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    static func hexColor(_ nk_hex: String) -> UIColor {
        let scanner = Scanner(string: nk_hex)
        if nk_hex.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue = CGFloat(b) / 255.0
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
    
    func toHexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb: Int = (Int)(r * 255) << 16 | (Int)(g * 255) << 8 | (Int)(b * 255) << 0
        return String(format: "#%06x", rgb)
    }
    
    /// 获取随机色
    static func randomColor() -> UIColor {
        let r = CGFloat(arc4random() % 256)
        let g = CGFloat(arc4random() % 256)
        let b = CGFloat(arc4random() % 256)
        return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1)
    }

    
    /**
     *  将颜色A变化到颜色B，可通过progress控制变化的程度
     *  @param fromColor 起始颜色
     *  @param toColor 目标颜色
     *  @param progress 变化程度，取值范围0.0f~1.0f
     */
    static func color(from fromColor: UIColor, to toColor: UIColor, progress: CGFloat) -> UIColor {
        let progress = min(progress, 1.0)
        let fromRed = fromColor.redValue
        let fromGreen = fromColor.greenValue
        let fromBlue = fromColor.blueValue
        let fromAlpha = fromColor.alphaValue
        
        let toRed = toColor.redValue
        let toGreen = toColor.greenValue
        let toBlue = toColor.blueValue
        let toAlpha = toColor.alphaValue
        
        let finalRed = fromRed + (toRed - fromRed) * progress
        let finalGreen = fromGreen + (toGreen - fromGreen) * progress
        let finalBlue = fromBlue + (toBlue - fromBlue) * progress
        let finalAlpha = fromAlpha + (toAlpha - fromAlpha) * progress
        
        return UIColor(red: finalRed, green: finalGreen, blue: finalBlue, alpha: finalAlpha)
    }
    
    
    
}

public extension  UIColor {
    
    func getRGB() -> (CGFloat, CGFloat, CGFloat) {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: nil)
        return (red * 255, green * 255, blue * 255)
    }
    
    
    /**
     *  获取当前UIColor对象里的红色色值
     *
     *  @return 红色通道的色值，值范围为0.0-1.0
     */
    var redValue: CGFloat {
        var r: CGFloat = 0
        if getRed(&r, green: nil, blue: nil, alpha: nil) {
            return r
        }
        return 0
    }
    
    /**
     *  获取当前UIColor对象里的绿色色值
     *
     *  @return 绿色通道的色值，值范围为0.0-1.0
     */
    var greenValue: CGFloat {
        var g: CGFloat = 0
        if getRed(nil, green: &g, blue: nil, alpha: nil) {
            return g
        }
        return 0
    }
    
    /**
     *  获取当前UIColor对象里的蓝色色值
     *
     *  @return 蓝色通道的色值，值范围为0.0-1.0
     */
    var blueValue: CGFloat {
        var b: CGFloat = 0
        if getRed(nil, green: nil, blue: &b, alpha: nil) {
            return b
        }
        return 0
    }
    /**
     *  获取当前UIColor对象里的透明色值
     *
     *  @return 透明通道的色值，值范围为0.0-1.0
     */
    var alphaValue: CGFloat {
        var a: CGFloat = 0
        if getRed(nil, green: nil, blue: nil, alpha: &a) {
            return a
        }
        return 0
    }
}
