//
//  UINavigationController+Extension.swift
//
//  Created by Nick on 2020/4/18.
//  Copyright © 2020 Nick. All rights reserved.
//

import UIKit

public extension NKFramework where Base: UINavigationController {
    /// push viewController并设置上级视图为指定的类型视图，查找指定类型视图的顺序为从前到后
    /// - Parameters:
    ///   - viewController: viewController
    ///   - superiorClass: 上级视图class
    ///   - animated: 是否展示动画
    func push(_ viewController: UIViewController, superiorClass: AnyClass, animated: Bool) {
        var viewControllers: [UIViewController] = []
        for controller in base.viewControllers {
            viewControllers.append(controller)
            if controller.isKind(of: superiorClass) { break }
        }
        viewControllers.append(viewController)
        base.setViewControllers(viewControllers, animated: animated)
    }

    /// push viewController，并pop指定数量的视图，popCount从当前视图算起
    /// - Parameters:
    ///   - viewController: viewController
    ///   - popCount: pop视图总数，eg: 跳转到下一个视图之前先pop两层视图 popCount = 2，popCount大于视图总数则pop到根视图
    ///   - animated: 是否展示动画
    func push(_ viewController: UIViewController, popCount: Int, animated: Bool) {
        var viewControllers: [UIViewController] = []
        if popCount >= base.viewControllers.count {
            viewController.addChild(base.viewControllers[0])
        } else {
            for i in 0 ..< base.viewControllers.count - popCount {
                viewControllers.append(base.viewControllers[i])
            }
        }
        viewControllers.append(viewController)
        base.setViewControllers(viewControllers, animated: animated)
    }

    /// 更新视图栈 - 从底部开始索引，直到找到指定viewController，如果未找到则不更新
    /// - Parameters:
    ///   - viewController: viewController
    ///   - animated: 是否展示动画
    func updateViewControllers(takeUntil viewController: UIViewController, animated: Bool) {
        guard let index = base.viewControllers.firstIndex(of: viewController) else { return }
        var viewControllers: [UIViewController] = []
        for i in 0 ... index {
            viewControllers.append(base.viewControllers[i])
        }
        base.setViewControllers(viewControllers, animated: animated)
    }

    /// 获取控制器 - 从控制器栈顶部开始索引
    /// - Parameter lastIndex: 如果超过控制器总数返回根控制器
    /// - Returns: viewController
    func viewController(at lastIndex: Int) -> UIViewController {
        if base.viewControllers.count > lastIndex {
            return base.viewControllers[base.viewControllers.count - lastIndex]
        }
        return base.viewControllers[0]
    }
}
