//
//  UINavigationBar+Extension.swift
//  Nick5683
//
//  Created by Nick5683 on 2023/12/23.
//  Copyright © 2017年 Nick5683. All rights reserved.
//

import UIKit

public extension UINavigationBar {
    
    private struct AssociatedKeys {
        static var nk_backImage = "nk_backImage"
    }

    var nk_backImage: UIImage? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.nk_backImage, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
        get { objc_getAssociatedObject(self, &AssociatedKeys.nk_backImage) as? UIImage }
    }

    func setNavTitleColor(color: UIColor) {
        
        if #available(iOS 13.0, *) {
            let appearance = self.standardAppearance
            // appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
            appearance.shadowImage = UIImage.init(color: UIColor.clear)  // 设置导航栏下边界分割线透明
            scrollEdgeAppearance = appearance;
            standardAppearance = appearance;
        } else {
            // 常规配置方式
            titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
            tintColor = color
        }
    }
    
    
    //MARK: -------------   返回按钮  -----------------
    func setNavBackImageColor(color:UIColor) {

        let backImage = self.nk_backImage?.imageWith(color: color)
        backIndicatorImage = backImage
        backIndicatorTransitionMaskImage = backImage
        tintColor = color //add
                
    }
    func setNavDefaultBackImageColor() {
        
        let backImage = self.nk_backImage?.withRenderingMode(.alwaysOriginal)
        backIndicatorImage = backImage
        backIndicatorTransitionMaskImage = backImage
        
    }
    //replaceStrToFunc
    
    func setNavBackBtnBlack() {
        
        let backImage = self.nk_backImage?.imageWith(color: UIColor.black)
        backIndicatorImage = backImage
        backIndicatorTransitionMaskImage = backImage
    }
    //MARK: -------------  navbar背景色  -----------------
    
    func setNavBarDefaultBgColor() {
        
        titleTextAttributes = {[NSAttributedString.Key.foregroundColor: UIColor.black]}()
        tintColor = UIColor.black
        barTintColor = UIColor.white
        setBackgroundImage(nil, for: .default)
        setNavBgColor(UIColor.white, UIColor.black)
        setNavBackBtnBlack()
    }
    
    func setNavBarBgColor(color:UIColor) {
        titleTextAttributes = {[NSAttributedString.Key.foregroundColor: UIColor.white]}()
        tintColor = UIColor.white
        barTintColor = color
        backgroundColor = color
        isTranslucent = false
        setNavBgColor(color, UIColor.white)
        
    }
    
    func setNavBarBgClear() {
        
        isTranslucent = true
        backgroundColor = UIColor.clear
        let clearImg = UIImage.createImage(color: UIColor.clear)
        setBackgroundImage(clearImg , for: .default)
        shadowImage = clearImg// 设置导航栏下边界分割线透明
        tintColor = UIColor.clear // item颜色
        barTintColor = UIColor.clear // 设置导航栏背景颜色
        //标题颜色
        titleTextAttributes = {[NSAttributedString.Key.foregroundColor: UIColor.black]}()
        setNavBgColor(UIColor.clear, UIColor.black)
    }
    
    
    func nk_setNavBarBgImage(_ image:UIImage) {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundImage = image
            appearance.shadowImage = UIImage.createImage(color: UIColor.clear)  // 设置导航栏下边界分割线透明
            
            self.scrollEdgeAppearance = appearance;
            self.standardAppearance = appearance;
        }
        self.setBackgroundImage(image, for: UIBarMetrics.default)
        
    }
    
    //replaceStrToFunc
    private func setNavBgColor(_ color:UIColor,_ titleColor:UIColor){
        let bgImg = UIImage.createImage(color: UIColor.clear)
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: titleColor]
            appearance.shadowImage = bgImg  // 设置导航栏下边界分割线透明
            appearance.backgroundColor = color;
            appearance.backgroundEffect = nil;
            self.scrollEdgeAppearance = appearance;
            self.standardAppearance = appearance;
        } else {
            // 常规配置方式
            
            self.setBackgroundImage(bgImg, for: .default)
        }
        
    }
}

 
