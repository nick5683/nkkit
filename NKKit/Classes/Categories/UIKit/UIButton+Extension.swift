//
//  UIButton+Extension.swift
//
//  Created by Nick on 2020/1/7.
//

import UIKit

fileprivate extension UIButton {
    private struct AssociatedKeys {
        static var tappedBlock = "tappedBlock"
    }

    @objc func tappedHandle() {
        tappedBlock?()
    }

    var tappedBlock: (() -> Void)? {
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.tappedBlock, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.tappedBlock) as? () -> Void
        }
    }
}

public extension NKFramework where Base: UIButton {
    func onTapped(_ block: @escaping () -> Void) {
        base.tappedBlock = block
        base.addTarget(base, action: #selector(UIButton.tappedHandle), for: .touchUpInside)
    }

    /// 使用此方法设置标题可避免system样式按钮标题闪烁
    func setTitle(_ title: String?, for state: UIControl.State) {
        base.titleLabel?.text = title
        base.setTitle(title, for: state)
    }
}
