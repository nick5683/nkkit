//
//  UIImage+Extension.swift
//
//  Created by Nick on 2019/11/13.
//

import UIKit

public extension UIImage {
    /// 通过颜色创建图片
    /// - Parameter color: 颜色
    static func createImage(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()!
        context.setFillColor(color.cgColor)
        context.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    //修改图片颜色为 color 的样子
    func imageWith(color: UIColor) -> UIImage {
        let width = size.width != 0 ? size.width : 1
        let height = size.height != 0 ? size.height : 1
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, kSCREEN_SCALE)
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: size.height)
        context?.scaleBy(x: 1, y: -1)
        context?.setBlendMode(.normal)
        
        let rect = CGRect(origin: .zero, size: size)
        context?.clip(to: rect, mask: cgImage!)
        color.setFill()
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? self
    }

    /// 生成一个渐变色图片
    /// - Parameter colors: 颜色
    /// - Parameter locations: 渐变节点
    /// - Parameter startPoint: 起始点，x = 0 - 1，y = 0 - 1
    /// - Parameter endPoint: 结束点，x = 0 - 1，y = 0 - 1
    static func createImage(colors: Array<UIColor>, locations: Array<CGFloat>, startPoint: CGPoint, endPoint: CGPoint) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 5, height: 5)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()!
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        var colorComponents: Array<CGFloat> = []
        for color in colors {
            colorComponents += color.cgColor.components!
        }
        let gradient = CGGradient(colorSpace: colorSpace, colorComponents: colorComponents, locations: locations, count: colors.count)!
        let startP = CGPoint(x: startPoint.x * rect.size.width, y: startPoint.y * rect.size.height)
        let endP = CGPoint(x: endPoint.x * rect.size.width, y: endPoint.y * rect.size.height)
        context.drawLinearGradient(gradient, start: startP, end: endP, options: .drawsBeforeStartLocation)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

    func reset(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let reSizeImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return reSizeImage
    }

    func reset(scale: CGFloat) -> UIImage {
        let size = CGSize(width: size.width / scale, height: size.height / scale)
        return reset(size: size)
    }

    func withTintColor(_ color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

    func withBlendColor(_ color: UIColor) -> UIImage {
        let image = withTintColor(color)!
        let filter = CIFilter(name: "CIColorBlendMode")
        filter?.setValue(CIImage(cgImage: cgImage!), forKey: kCIInputBackgroundImageKey)
        filter?.setValue(CIImage(cgImage: image.cgImage!), forKey: kCIInputImageKey)
        let outputImage = filter?.outputImage
        let context = CIContext(options: nil)
        let imageRef = context.createCGImage(outputImage!, from: outputImage!.extent)
        let resultImage = UIImage(cgImage: imageRef!, scale: scale, orientation: imageOrientation)
        return resultImage
    }
}
