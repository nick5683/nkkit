//
//  WebViewController.swift
//
//  Created by Nick on 2019/12/10.
//

import UIKit
import WebKit

open class WebViewController: UIViewController {
    /// 从链接加载
    open var url: URL? {
        didSet {
            guard let url = url, isViewLoaded else { return }
            webView.load(URLRequest(url: url))
        }
    }

    /// 加载HTML字符串
    open var html: String? {
        didSet {
            guard let html = html, isViewLoaded else { return }
            webView.loadHTMLString(html, baseURL: nil)
        }
    }

    /// 导航栏标题
    public var navTitle: String? {
        didSet { title = navTitle }
    }

    public var webView: WKWebView!

    public private(set) var progressView: UIProgressView!

    public var showProgress:Bool = false
    public var swipeBackEnable:Bool = false
    // MARK: - KVO

    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.alpha = 1.0
            progressView.progress = Float(webView.estimatedProgress)
            if progressView.progress == 1 {
                UIView.animate(withDuration: 0.2) {
                    self.progressView.alpha = 0
                }
            }
        } else if keyPath == "title" {
            if navTitle == nil {
                title = webView.title
            }
        } else if keyPath == "canGoBack" {
            if let newValue = change?[NSKeyValueChangeKey.newKey], let newV = newValue as? Bool{
                if newV == true{
                    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false;
                }else{
                    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
                }
            }
        }
    }

    // MARK: - Override

    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var y: CGFloat = 0
        if extendedLayoutIncludesOpaqueBars ||
            navigationController?.navigationBar.isTranslucent ?? false {
            y = UIApplication.shared.statusBarFrame.size.height + 44
        }
        progressView.frame = CGRect(x: 0, y: y, width: view.bounds.width, height: 2)
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        initSubviews()
        if let url = url {
            webView.load(URLRequest(url: url))
        } else if let html = html {
            webView.loadHTMLString(html, baseURL: nil)
        }
    }

    deinit {
        webView.removeObserver(self, forKeyPath: "title", context: nil)
        if showProgress{
            webView.removeObserver(self, forKeyPath: "estimatedProgress", context: nil)
        }
        if swipeBackEnable{
            webView.removeObserver(self, forKeyPath: "canGoBack", context: nil)
        }

    }

    // MARK: - Init

    open func initSubviews() {
        view.backgroundColor = .white
        
        let webConfig = WKWebViewConfiguration()
        webConfig.userContentController = WKUserContentController()
        let jsStr = """
        var script = document.createElement('meta');
        script.name = 'viewport';
        script.content="width=device-width, initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0, user-scalable=no";
        document.getElementsByTagName('head')[0].appendChild(script);
        document.documentElement.style.webkitTouchCallout='none';
        document.documentElement.style.webkitUserSelect='none';
        """
        let wkScript = WKUserScript(source: jsStr, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        webConfig.userContentController.addUserScript(wkScript)
        webView = WKWebView(frame: view.bounds, configuration: webConfig)
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.scrollView.showsHorizontalScrollIndicator = false
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.addObserver(self, forKeyPath: "title", options: .new, context: nil)
        if showProgress{
            webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        }
        if swipeBackEnable{
            webView.addObserver(self, forKeyPath: "canGoBack", options: .new, context: nil)
        }
        progressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 2))
        progressView.backgroundColor = .white
        progressView.trackTintColor = .white

        view.addSubview(webView)
        view.addSubview(progressView)
    }
}
