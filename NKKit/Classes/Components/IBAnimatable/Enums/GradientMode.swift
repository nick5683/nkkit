//
//  GradientMode.swift
//
//  Created by Nick on 2020/6/11.
//

import Foundation

public enum GradientMode: String, IBEnum {
  case linear
  case radial
  case conical
}

#if swift(>=4.2)
extension GradientMode: CaseIterable {}
#endif
