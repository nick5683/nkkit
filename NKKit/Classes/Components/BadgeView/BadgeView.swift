//
//  BadgeView.swift
//
//  Created by Nick on 2019/12/16.
//

import UIKit

public class BadgeView: UIView {
    /// 背景颜色
    public var color: UIColor = .red {
        didSet { super.backgroundColor = color }
    }

    /// 文字字体
    public var font: UIFont = UIFont.systemFont(ofSize: 12) {
        didSet { lblTitle.font = font }
    }

    /// 字体颜色
    public var textColor: UIColor = .white {
        didSet { lblTitle.textColor = textColor }
    }

    /// 标题
    public var title: String? {
        didSet { lblTitle.text = title }
    }

    /// 左右间距
    public var spacingLR: CGFloat = 5
    /// 自适应宽度
    public var isAutoWidth: Bool = true

    var lblTitle: UILabel = UILabel()
    var contentWidth: CGFloat = 0

    public override var backgroundColor: UIColor? {
        set { /* 阻断某些系统控件设置主题色 */ }
        get { return super.backgroundColor }
    }

    // MARK: - Override

    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height * 0.5
        lblTitle.sizeToFit()
        if isAutoWidth {
            contentWidth = max(lblTitle.bounds.width + spacingLR * 2, bounds.height)
            invalidateIntrinsicContentSize()
        }
        lblTitle.center = CGPoint(x: bounds.midX, y: bounds.midY)
    }

    public override var intrinsicContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentWidth)
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initSubviews() {
        lblTitle.font = font
        lblTitle.textColor = textColor
        lblTitle.textAlignment = .center
        addSubview(lblTitle)

        super.backgroundColor = color
    }
}
