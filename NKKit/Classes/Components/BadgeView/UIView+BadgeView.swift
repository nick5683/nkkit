//
//  UIView+BadgeView.swift
//
//  Created by Nick on 2019/12/16.
//

import UIKit
import SnapKit

public enum BadgeViewCorner {
    /// 左上角
    case topLeft
    /// 右上角
    case topRight
    /// 左下角
    case bottomLeft
    /// 右下角
    case bottomRight
    /// 中间偏左
    case centerLeft
    /// 中间偏右
    case centerRight
    /// 中心
    case center
}

fileprivate extension UIView {
    private struct AssociatedKeys {
        static var badgeView = "badgeView"
    }

    var badgeView: BadgeView? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.badgeView, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
        get { objc_getAssociatedObject(self, &AssociatedKeys.badgeView) as? BadgeView }
    }
}

public extension NKFramework where Base: UIView {
    /// 显示小圆点提示
    /// - Parameters:
    ///   - corner: 位置
    @discardableResult
    func showBadge(at corner: BadgeViewCorner, size: CGFloat, offset: CGPoint = .zero, isAutoWidth: Bool = false) -> BadgeView {
        if base.badgeView != nil { clearBadge() }
        let badgeView = BadgeView()
        badgeView.isAutoWidth = isAutoWidth
        badgeView.layer.zPosition = 999
        base.addSubview(badgeView)
        setConstraints(badgeView: badgeView, corner: corner, size: size, offset: offset, isAutoWidth: isAutoWidth)
        base.badgeView = badgeView
        return badgeView
    }

    func setConstraints(badgeView: BadgeView, corner: BadgeViewCorner, size: CGFloat, offset: CGPoint = .zero, isAutoWidth: Bool = false) {
        badgeView.snp.makeConstraints { make in
            switch corner {
            case .topLeft:
                make.top.equalToSuperview().offset(offset.y)
                make.left.equalToSuperview().offset(offset.x)
            case .topRight:
                make.top.equalToSuperview().offset(offset.y)
                make.right.equalToSuperview().offset(offset.x)
            case .bottomLeft:
                make.left.equalToSuperview().offset(offset.x)
                make.bottom.equalToSuperview().offset(offset.y)
            case .bottomRight:
                make.right.equalToSuperview().offset(offset.x)
                make.bottom.equalToSuperview().offset(offset.y)
            case .centerLeft:
                make.centerY.equalToSuperview().offset(offset.y)
                make.left.equalToSuperview().offset(offset.x)
            case .centerRight:
                make.centerY.equalToSuperview().offset(offset.y)
                make.right.equalToSuperview().offset(offset.x)
            case .center:
                make.centerX.equalToSuperview().offset(offset.x)
                make.centerY.equalToSuperview().offset(offset.y)
            }
            make.height.equalTo(size)
            if !isAutoWidth { make.width.equalTo(size) }
        }
    }

    /// 清除小圆点提示
    func clearBadge() {
        base.badgeView?.removeFromSuperview()
        base.badgeView = nil
    }
}
