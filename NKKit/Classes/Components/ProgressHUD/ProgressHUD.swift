//
//  ProgressHUD.swift
//
//  Copyright © 2019 Nick. All rights reserved.
//

import MBProgressHUD
import UIKit

extension UIView {
    public static var nk_configHUDBlock: ((_ hud: MBProgressHUD) -> Void)?
    private struct AssociatedKeys {
        static var loadingHUD = "loadingHUD"
        static var loadingHUDCounter = "loadingHUDCounter"
    }

    fileprivate var loadingHUD: MBProgressHUD? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.loadingHUD, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.loadingHUD) as? MBProgressHUD }
    }

    fileprivate var loadingHUDCounter: Int {
        set { objc_setAssociatedObject(self, &AssociatedKeys.loadingHUDCounter, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
        get { return (objc_getAssociatedObject(self, &AssociatedKeys.loadingHUDCounter) as? Int) ?? 0 }
    }
}

public extension NKFramework where Base: UIView {
    /// 显示loadingHUD
    /// - Parameter text: 提示文本
    func showHUDLoading(text: String? = nil) {
        var hud = base.loadingHUD
        if hud == nil {
            hud = MBProgressHUD.showAdded(to: base, animated: true)
            base.loadingHUD = hud
            base.loadingHUDCounter += 1
        } else if hud!.mode == .indeterminate {
            base.loadingHUDCounter += 1
        }
        hud?.mode = .indeterminate
        hud?.detailsLabel.text = text
        UIView.nk_configHUDBlock?(hud!)
    }

    /// 显示进度HUD
    /// - Parameters:
    ///   - progress: 进度
    ///   - text: 提示文本
    func showHUDProgress(_ progress: Float, text: String? = nil) {
        var hud = base.loadingHUD
        if hud == nil {
            hud = MBProgressHUD.showAdded(to: base, animated: true)
            base.loadingHUD = hud
            base.loadingHUDCounter += 1
        }
        hud?.mode = .annularDeterminate
        hud?.detailsLabel.text = text
        hud?.progress = progress
        UIView.nk_configHUDBlock?(hud!)
    }

    /// 显示纯文本HUD
    /// - Parameters:
    ///   - text: 提示文本
    ///   - second: 显示时长，默认2s隐藏
    func showHUDText(_ text: String, second: Int = 2) {
        if text.count<=0{
            return
        }
        let hud = MBProgressHUD.showAdded(to: base, animated: true)
        hud.isUserInteractionEnabled = false
        hud.mode = .text
        hud.detailsLabel.text = text
        hud.hide(animated: true, afterDelay: TimeInterval(second))
        UIView.nk_configHUDBlock?(hud)
    }

    /// 隐藏HUD
    func hideHUD() {
        base.loadingHUDCounter -= 1
        if base.loadingHUDCounter <= 0 {
            base.loadingHUDCounter = 0
            base.loadingHUD?.hide(animated: true)
            base.loadingHUD = nil
        }
    }
}
