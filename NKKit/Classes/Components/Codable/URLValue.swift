//
//  URLValue.swift
//
//  Created by Haijun on 2019/11/22.
//

import Foundation

@propertyWrapper
public class URLValue: Codable {
    public var wrappedValue: URL
    
    public init(_ wrappedValue: URL) {
        self.wrappedValue = wrappedValue
    }

    public required init(from decoder: Decoder) throws {
        let urlString = try String(from: decoder)
        wrappedValue = URL(string: urlString)!
    }

    public func encode(to encoder: Encoder) throws {
        let string = String(describing: wrappedValue)
        try string.encode(to: encoder)
    }
}
