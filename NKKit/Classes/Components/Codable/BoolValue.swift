//
//  BoolValue.swift
//
//  Created by Haijun on 2019/11/26.
//

import Foundation

@propertyWrapper
public class BoolValue: Codable {
    public var wrappedValue: Bool
    private let value: Codable

    public required init(from decoder: Decoder) throws {
        if let rawValue = try? String(from: decoder) {
            value = rawValue
            wrappedValue = false
            if rawValue == "0" {
                wrappedValue = false
            } else if rawValue == "1" {
                wrappedValue = true
            } else if rawValue == "true" {
                wrappedValue = true
            }
        } else if let rawValue = try? Int(from: decoder) {
            value = rawValue
            wrappedValue = rawValue == 1
        } else {
            print("解析Bool值出错")
            value = 0
            wrappedValue = false
        }
    }

    public func encode(to encoder: Encoder) throws {
        try value.encode(to: encoder)
    }
}
