//
//  UIViewController+DWVideo.swift
//
//  Created by Nick on 2019/12/10.
//

import TZImagePickerController
import UIKit

public extension NKFramework where Base: UIViewController {
    /// 选择视频
    /// - Parameter block: 回调
    func chooseVideo(block: @escaping (UIImage, PHAsset) -> Void) {
        let imagePicker = TZImagePickerController()
        imagePicker.allowTakePicture = false
        imagePicker.allowTakeVideo = true
        imagePicker.allowPickingImage = false
        imagePicker.sortAscendingByModificationDate = false
        imagePicker.didFinishPickingVideoHandle = { coverImage, asset in
            block(coverImage!, asset!)
        }
        imagePicker.modalPresentationStyle = .fullScreen
        base.present(imagePicker, animated: true, completion: nil)
    }
}
