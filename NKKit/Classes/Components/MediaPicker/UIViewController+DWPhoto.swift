//
//  UIViewController+DWPhoto.swift
//
//  Created by Nick on 2019/12/10.
//

import TZImagePickerController
import UIKit

public extension NKFramework where Base: UIViewController {
    /// 选择单张照片
    /// - Parameters:
    ///   - isNeedCrop: 是否需要剪裁
    ///   - block: 回调
    func choosePhoto(isNeedCrop: Bool = false, block: @escaping (UIImage) -> Void) {
        choosePhoto(isNeedCrop: isNeedCrop, cropRect: .zero, isCircleCrop: false, block: block)
    }

    /// 选择单张照片，指定剪裁比例
    /// - Parameters:
    ///   - cropAspectRatio: 剪裁比例
    ///   - block: 回调
    func choosePhoto(cropRect: CGRect, block: @escaping (UIImage) -> Void) {
        choosePhoto(isNeedCrop: true, cropRect: cropRect, isCircleCrop: false, block: block)
    }

    /// 选择单张照片，圆形剪裁
    /// - Parameter block: 回调
    func choosePhotoUseCircleCrop(block: @escaping (UIImage) -> Void) {
        choosePhoto(isNeedCrop: true, cropRect: .zero, isCircleCrop: true, block: block)
    }

    /// 选择多张照片
    /// - Parameters:
    ///   - count: 最大图片数
    ///   - block: 回调
    func choosePhotos(count: Int, block: @escaping ([UIImage]) -> Void) {
        let imagePicker = TZImagePickerController()
        imagePicker.maxImagesCount = count
        imagePicker.allowTakePicture = true
        imagePicker.allowTakeVideo = false
        imagePicker.allowPickingVideo = false
        imagePicker.sortAscendingByModificationDate = false
        imagePicker.didFinishPickingPhotosHandle = { photos, _, _ in
            block(photos!)
        }
        imagePicker.modalPresentationStyle = .fullScreen
        base.present(imagePicker, animated: true, completion: nil)
    }

    /// 拍照
    /// - Parameters:
    ///   - isNeedCrop: 是否需要剪裁
    ///   - block: 回调
    func choosePhotoFromCamera(isNeedCrop: Bool = false, block: @escaping (UIImage) -> Void) {
        choosePhotoFromCamera(isNeedCrop: isNeedCrop, cropRect: .zero, isCircleCrop: false, block: block)
    }

    /// 从相册选取照片
    private func choosePhoto(isNeedCrop: Bool, cropRect: CGRect, isCircleCrop: Bool, block: @escaping (UIImage) -> Void) {
        let actions = ["打开相机", "从相册选择", "取消"]
        base.nk.showAlert(title: nil, message: nil, style: .actionSheet, actions: actions, cancelIndex: 2, destructiveIndex: -1) { index in
            if index == 0 {
                self.choosePhotoFromCamera(isNeedCrop: isNeedCrop, cropRect: cropRect, isCircleCrop: isCircleCrop, block: block)
            } else if index == 1 {
                let imagePicker = TZImagePickerController()
                imagePicker.maxImagesCount = 1
                imagePicker.showSelectBtn = false
                imagePicker.allowTakePicture = true
                imagePicker.allowTakeVideo = false
                imagePicker.allowPickingVideo = false
                imagePicker.allowCrop = isNeedCrop
                imagePicker.cropRect = cropRect
                imagePicker.needCircleCrop = isCircleCrop
                imagePicker.sortAscendingByModificationDate = false
                imagePicker.didFinishPickingPhotosHandle = { photos, _, _ in
                    if let photo = photos?.first {
                        block(photo)
                    }
                    self.base.dismiss(animated: true, completion: nil)
                }
                imagePicker.modalPresentationStyle = .fullScreen
                self.base.present(imagePicker, animated: true, completion: nil)
            }
        }
    }

    private func choosePhotoFromCamera(isNeedCrop: Bool, cropRect: CGRect, isCircleCrop: Bool, block: @escaping (UIImage) -> Void) {
        let sourceType = UIImagePickerController.SourceType.camera
        if !UIImagePickerController.isSourceTypeAvailable(sourceType) {
            print("模拟器中无法打开照相机,请在真机中使用")
            return
        }
        let imagePicker = TZImagePickerController()
        imagePicker.maxImagesCount = 1
        imagePicker.showSelectBtn = false
        imagePicker.allowTakePicture = true
        imagePicker.allowTakeVideo = true
        imagePicker.allowPickingVideo = false
        imagePicker.allowCrop = isNeedCrop
        imagePicker.cropRect = cropRect
        imagePicker.needCircleCrop = isCircleCrop
        imagePicker.sortAscendingByModificationDate = false
        imagePicker.didFinishPickingPhotosHandle = { photos, _, _ in
            if let photo = photos?.first {
                block(photo)
            }
            self.base.dismiss(animated: true, completion: nil)
        }
        imagePicker.modalPresentationStyle = .fullScreen
        base.present(imagePicker, animated: true, completion: nil)
    }
}
