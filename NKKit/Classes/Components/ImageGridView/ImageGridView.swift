//
//  ImageGridView.swift
//
//  Created by Nick on 2019/12/10.
//

import Kingfisher
import UIKit

public protocol ImageGridViewImage {
    var imageObject: Any { get }
}


public protocol ImageGridViewDelegate: NSObjectProtocol {
    func imageGridViewAddDidClick(_ imageGridView: ImageGridView)
    func imageGridViewImageDidChange(_ imageGridView: ImageGridView)
}

public extension ImageGridViewDelegate {
    func imageGridViewAddDidClick(_ imageGridView: ImageGridView) {}
    func imageGridViewImageDidChange(_ imageGridView: ImageGridView) {}
}

public class ImageGridView: UIView, ImageGridViewCellDelegate {
    public weak var delegate: ImageGridViewDelegate?
    public private(set) var images: [Any] = []
    public var numberOfImages: Int {
        return images.count
    }

    /// 行数，默认 = 0，0 = 无限制
    public var numberOfRows: Int = 0
    /// 列数，默认 = 3
    public var numberOfColumns: Int = 3
    /// 行间距，默认 = 10
    public var rowSpacing: CGFloat = 10
    /// 列间距，默认 = 10
    public var columnSpacing: CGFloat = 10
    /// 宽高比，默认 1:1
    public var aspectRatio: CGFloat = 1
    /// 图片圆角，默认 = 0
    public var imageRadius: CGFloat = 0
    /// 圆角是否只作用于图片，如果 true，deleteImage不会被圆角影响
    public var radiusOnlyImage: Bool = false

    /// 最大图片数，默认 = 0
    @IBInspectable public var maxImageCount: Int = 0
    /// 是否可编辑（增加/删除），默认 = NO
    @IBInspectable public var isCanEdit: Bool = false
    /// 添加图片
    @IBInspectable public var addImage: UIImage? {
        didSet { addImageView.image = addImage }
    }
    /// 添加图片展示模式
    public var addImageContentMode: UIView.ContentMode = .scaleToFill {
        didSet { addImageView.contentMode = addImageContentMode }
    }

    /// 删除图片
    @IBInspectable public var deleteImage: UIImage?
    /// 删除图片距离偏移
    public var deleteImageOffset: UIOffset = .zero
    /// 默认占位图
    @IBInspectable public var placeholderImage: UIImage?
    /// 占位图显示方式
    public var placeholderImageMode: UIView.ContentMode?
    /// 占位图背景色
    public var placeholderBackColor: UIColor?
    /// 是否可以浏览图片（点击查看大图），默认 = YES
    @IBInspectable public var isAllowBrowseImage: Bool = true

    /// 内容大小更新回调
    public var didUpdateContentSize: (() -> Void)?

    var imageViews: [ImageGridViewCell] = []
    lazy var addImageView: UIImageView = {
        let view = UIImageView(image: self.addImage)
        addSubview(view)
        return view
    }()

    var contentHeight: CGFloat = 0

    // MARK: - Events

    public func reload(_ images: [Any]) {
        self.images = []
        self.images.append(contentsOf: images)
        imageViews.forEach { $0.removeFromSuperview() }
        imageViews.removeAll()
        imageViews.append(contentsOf: images.map { cellWithImage($0) })
        contentHeight = calculateContentHeight(images, width: bounds.width)
        invalidateIntrinsicContentSize()
        setNeedsLayout()
    }

    public func insert(_ image: Any, at index: Int) {
        images.insert(image, at: index)
        imageViews.insert(cellWithImage(image), at: index)
        didChangeImages()
    }

    public func delete(at index: Int) {
        images.remove(at: index)
        imageViews[index].removeFromSuperview()
        imageViews.remove(at: index)
        didChangeImages()
    }

    public func add(_ images: [Any]) {
        self.images.append(contentsOf: images)
        imageViews.append(contentsOf: images.map { cellWithImage($0) })
        didChangeImages()
    }

    func didChangeImages() {
        setNeedsLayout()
        layoutIfNeeded()
        delegate?.imageGridViewImageDidChange(self)
    }

    public func calculateContentHeight(_ images: [Any], width: CGFloat) -> CGFloat {
        var imagesCount = imageViews.count
        let hasAdd = isCanEdit && images.count < maxImageCount
        if hasAdd { imagesCount += 1 }
        let numberOfRows = self.numberOfRows > 0 ? self.numberOfRows : Int(ceil(Double(imagesCount) / Double(numberOfColumns)))
        let layoutWidth = (width - columnSpacing * CGFloat(numberOfColumns - 1)) / CGFloat(numberOfColumns)
        let height = layoutWidth / aspectRatio
        var lastFrame = CGRect.zero
        for i in 0 ..< numberOfRows {
            for j in 0 ..< Int(numberOfColumns) {
                let idx = i * numberOfColumns + j
                let frame = CGRect(x: CGFloat(j) * (width + columnSpacing), y: CGFloat(i) * (height + rowSpacing), width: width, height: height)
                if hasAdd && idx == imagesCount - 1 {
                    lastFrame = frame
                    break
                }
                if idx >= imagesCount { break }
                lastFrame = frame
            }
        }
        return lastFrame.maxY
    }

    // MARK: - ImageGridViewCellDelegate

    func imageGridViewCellDeleteDidClick(_ cell: ImageGridViewCell) {
        guard let idx = imageViews.firstIndex(of: cell) else { return }
        delete(at: idx)
    }

    // MARK: - Override

    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        let location = touches.first?.location(in: self) ?? .zero
        if isCanEdit && images.count < maxImageCount && addImageView.frame.contains(location) {
            delegate?.imageGridViewAddDidClick(self)
            return
        }
        if !isAllowBrowseImage { return }
        for i in 0 ..< imageViews.count {
            if imageViews[i].frame.contains(location) {
                ImageBrowser.show(images.enumerated().map({ (idx, obj) -> ImageBrowserItem in
                    var imageObject = obj
                    var item: ImageBrowserItem!
                    if let image = obj as? ImageGridViewImage {
                        imageObject = image.imageObject
                    }
                    if let image = imageObject as? UIImage {
                        item = ImageBrowserItem(image, owner: imageViews[idx])
                    } else if let image = imageObject as? URL {
                        item = ImageBrowserItem(image, owner: imageViews[idx])
                    } else if let image = imageObject as? String, let imageURL = URL(string: image) {
                        item = ImageBrowserItem(imageURL, owner: imageViews[idx])
                    }
                    return item
                }), currentPage: i, triggerView: imageViews[i])
                break
            }
        }
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        var imagesCount = imageViews.count
        let hasAdd = isCanEdit && images.count < maxImageCount
        if hasAdd {
            addImageView.isHidden = false
            imagesCount += 1
        } else {
            addImageView.isHidden = true
        }
        let numberOfRows = self.numberOfRows > 0 ? self.numberOfRows : Int(ceil(Double(imagesCount) / Double(numberOfColumns)))
        let width = (bounds.width - columnSpacing * CGFloat(numberOfColumns - 1)) / CGFloat(numberOfColumns)
        let height = width / aspectRatio
        for i in 0 ..< numberOfRows {
            for j in 0 ..< Int(numberOfColumns) {
                let idx = i * numberOfColumns + j
                let frame = CGRect(x: CGFloat(j) * (width + columnSpacing), y: CGFloat(i) * (height + rowSpacing), width: width, height: height)
                if hasAdd && idx == imagesCount - 1 {
                    addImageView.frame = frame
                    break
                }
                if idx >= imagesCount { break }
                imageViews[idx].frame = frame
                imageViews[idx].deleteButton.isHidden = !isCanEdit
            }
        }
        let oldContentHeight = contentHeight
        contentHeight = hasAdd ? addImageView.frame.maxY : (imageViews.last?.frame.maxY ?? 0)
        if oldContentHeight != contentHeight {
            invalidateIntrinsicContentSize()
            didUpdateContentSize?()
        }
    }

    public override var intrinsicContentSize: CGSize {
        return CGSize(width: bounds.width, height: contentHeight)
    }
    
    override public func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        for view in subviews {
            if let cell = view as? ImageGridViewCell {
                let point = cell.deleteButton.convert(point, from: self)
                if cell.deleteButton.point(inside: point, with: event) {
                    return cell.deleteButton
                }
            }
        }
        return super.hitTest(point, with: event)
    }

    // MARK: - Helper

    func cellWithImage(_ image: Any) -> ImageGridViewCell {
        let cell = ImageGridViewCell()
        cell.delegate = self
        cell.deleteButton.isHidden = !isCanEdit
        cell.deleteButtonOffset = deleteImageOffset
        if radiusOnlyImage {
            cell.imageView.layer.cornerRadius = imageRadius
            cell.imageView.clipsToBounds = true
        } else {
            cell.layer.cornerRadius = imageRadius
            cell.clipsToBounds = true
        }
        cell.deleteButton.setImage(deleteImage, for: .normal)
        var imageObject = image
        if let image = imageObject as? ImageGridViewImage {
            imageObject = image.imageObject
        }
        if let image = imageObject as? UIImage {
            cell.imageView.image = image
        } else if let image = imageObject as? URL {
            cell.imageView.setImage(with: image, placeholder: placeholderImage, placeholderImageMode: placeholderImageMode, placeholderBackColor: placeholderBackColor)
        } else if let image = imageObject as? String, let imageURL = URL(string: image) {
            cell.imageView.setImage(with: imageURL, placeholder: placeholderImage, placeholderImageMode: placeholderImageMode, placeholderBackColor: placeholderBackColor)
        }
        addSubview(cell)
        return cell
    }
}

protocol ImageGridViewCellDelegate: NSObjectProtocol {
    func imageGridViewCellDeleteDidClick(_ cell: ImageGridViewCell)
}

class ImageGridViewCell: UIView {
    weak var delegate: ImageGridViewCellDelegate?
    var imageView: UIImageView!
    var deleteButton: UIButton!
    var deleteButtonOffset: UIOffset = .zero
    // MARK: - Events

    @objc func onDeleteClick() {
        delegate?.imageGridViewCellDeleteDidClick(self)
    }

    // MARK: - Override

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = bounds
        deleteButton.frame.origin.y = 0 + deleteButtonOffset.vertical
        deleteButton.frame.size.width = bounds.width * 0.3
        deleteButton.frame.size.height = bounds.height * 0.3
        deleteButton.frame.origin.x = bounds.width - deleteButton.frame.width +  deleteButtonOffset.horizontal
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initSubviews() {
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        addSubview(imageView)

        deleteButton = UIButton(type: .custom)
        deleteButton.contentVerticalAlignment = .top
        deleteButton.contentHorizontalAlignment = .right
        deleteButton.addTarget(self, action: #selector(onDeleteClick), for: .touchUpInside)
        addSubview(deleteButton)
    }
}

private extension UIImageView {
    func setImage(with resource: Resource?, placeholder: UIImage? = nil, placeholderImageMode: UIView.ContentMode? = nil, placeholderBackColor: UIColor? = nil) {
        if placeholderImageMode != nil { contentMode = placeholderImageMode! }
        if placeholderBackColor != nil { backgroundColor = placeholderBackColor }
        kf.setImage(with: resource, placeholder: placeholder) { [weak self] r in
            switch r {
            case .success(_):
                let animation = CABasicAnimation(keyPath: "opacity")
                animation.fromValue = 0
                animation.toValue = 1
                animation.duration = 0.5
                self?.layer.add(animation, forKey: nil)
            case .failure(_):
                if let placeholderImageMode = placeholderImageMode {
                    self?.contentMode = placeholderImageMode
                    if placeholderBackColor != nil {
                        self?.backgroundColor = placeholderBackColor
                    }
                }
            }
        }
    }
}
