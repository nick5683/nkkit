//
//  MessageHUD.swift
//
//  Created by Haijun on 2019/11/18.
//

import UIKit

public class MessageHUD {
    static var window: UIWindow?
    static var lastHUDView: UIView?
    static var counter: Int = 0

    /// 显示消息提示框
    /// - Parameters:
    ///   - message: 消息内容
    ///   - icon: 图标
    ///   - backgroundColor: 背景颜色
    public static func show(_ message: String, icon: UIImage? = nil, backgroundColor: UIColor) {
        createWindowIfNeed()
        guard let window = self.window else { return }
        if lastHUDView != nil {
            lastHUDView?.removeFromSuperview()
            lastHUDView = nil
        }
        counter += 1
        let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        let contentHeight: CGFloat = 44
        let height = statusBarHeight + contentHeight
        let hudView = MessageHUDView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0))
        hudView.messageLabel.text = message
        hudView.iconView.image = icon
        hudView.topSpacing = statusBarHeight
        hudView.contentHeight = contentHeight
        hudView.backgroundColor = backgroundColor
        window.addSubview(hudView)
        lastHUDView = hudView
        UIView.animate(withDuration: 0.2, animations: {
            hudView.frame.size.height = height + 10
        }) { _ in
            UIView.animate(withDuration: 0.1) {
                hudView.frame.size.height = height
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.hide(hudView: hudView, height: height)
        }
    }

    private static func hide(hudView: UIView, height: CGFloat) {
        UIView.animate(withDuration: 0.1, animations: {
            hudView.frame.size.height = height + 10
        }) { _ in
            UIView.animate(withDuration: 0.2, animations: {
                hudView.frame.size.height = 0
            }) { _ in
                hudView.removeFromSuperview()
                if self.lastHUDView == hudView {
                    self.lastHUDView = nil
                }
                self.counter -= 1
                if self.counter <= 0 {
                    self.window = nil
                }
            }
        }
    }

    private static func createWindowIfNeed() {
        if window != nil { return }
        let alertWindow = HUDWindow(frame: UIScreen.main.bounds)
        alertWindow.backgroundColor = UIColor.clear
        alertWindow.rootViewController = MessageHUDViewController()
        if #available(iOS 13.0, *) {
            if let currentWindowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
                alertWindow.windowScene = currentWindowScene
            }
        }
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.view.frame = CGRect.zero
        window = alertWindow
    }
}

private class HUDWindow: UIWindow {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return nil
    }
}

private class MessageHUDViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

private class MessageHUDView: UIView {
    var iconView: UIImageView!
    var messageLabel: UILabel!
    var contentView: UIView!
    var contentHeight: CGFloat = 0
    var topSpacing: CGFloat = 0

    // MARK: - Override

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = CGRect(x: 0, y: topSpacing, width: bounds.width, height: contentHeight)
        var x: CGFloat = 20
        if iconView.image != nil { x += 38 }
        var textFrame = messageLabel.textRect(forBounds: CGRect(x: 0, y: 0, width: bounds.width - x - 20, height: 999), limitedToNumberOfLines: 2)
        textFrame.origin.x = x
        textFrame.size.width = bounds.width - x - 20
        textFrame.origin.y = (contentView.bounds.height - textFrame.height) * 0.5
        messageLabel.frame = textFrame
        iconView.isHidden = iconView.image == nil
        iconView.frame = CGRect(x: 20, y: 0, width: 30, height: 30)
        iconView.center.y = messageLabel.center.y
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
        clipsToBounds = true
        backgroundColor = UIColor.white
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initSubviews() {
        contentView = UIView()
        addSubview(contentView)

        iconView = UIImageView()
        contentView.addSubview(iconView)

        messageLabel = UILabel()
        messageLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        messageLabel.textColor = UIColor.white
        messageLabel.numberOfLines = 2
        contentView.addSubview(messageLabel)
    }
}
