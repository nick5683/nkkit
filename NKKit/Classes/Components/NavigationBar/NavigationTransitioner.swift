//
//  NavigationTransitioner.swift
//
//  Created by Nick on 2019/11/14.
//

import UIKit

class NavigationTransitioner: NSObject {
    lazy var fromViewControllerFakeBar = FakeNavigationBar()
    lazy var toViewControllerFakeBar = FakeNavigationBar()

    // MARK: - Core

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if !viewController.extendedLayoutIncludesOpaqueBars { viewController.extendedLayoutIncludesOpaqueBars = true }
        let navigationBar = navigationController.navigationBar
        let currentItem = viewController.navigationItem
        let currentConfig = navigationBar.currentNavigationConfig ?? NavigationConfiguration.defaultConfig
        guard let showConfig = viewController.nk.navigationConfig else { return }

        if showConfig.isHidden != navigationController.isNavigationBarHidden {
            navigationController.setNavigationBarHidden(showConfig.isHidden, animated: animated)
        }
        let isNeedFakeBar = needFakeBar(currentConfig: currentConfig, showConfig: showConfig)
        var transparentConfig: NavigationConfiguration?
        if isNeedFakeBar {
            transparentConfig = showConfig.copy() as? NavigationConfiguration
            transparentConfig?.shadowColor = nil
            transparentConfig?.isTransparent = true
        }

        if showConfig.isHidden {
            navigationBar.adjustTintColor(config: currentConfig)
        } else {
            navigationBar.applyConfiguration(config: transparentConfig ?? showConfig)
            navigationBar.adjustTitleColor(config: transparentConfig ?? showConfig, item: currentItem)
        }
        if !animated { return }
        navigationController.transitionCoordinator?.animate(alongsideTransition: { context in
            if !isNeedFakeBar { return }
            UIView.setAnimationsEnabled(false)
            let toVC = context.viewController(forKey: .to)
            let fromVC = context.viewController(forKey: .from)
            if fromVC != nil && currentConfig.isBarBackVisible {
                guard let fakeBarFrame = fromVC?.nk.fakeBarFrameForNavigationBar(navigationBar: navigationBar) else {
                    return
                }
                let fakeBar = self.fromViewControllerFakeBar
                fakeBar.applyConfiguration(config: currentConfig)
                fakeBar.frame = fakeBarFrame
                fromVC?.view.addSubview(fakeBar)
            }
            if toVC != nil && showConfig.isBarBackVisible {
                guard var fakeBarFrame = toVC?.nk.fakeBarFrameForNavigationBar(navigationBar: navigationBar) else {
                    return
                }
                if toVC!.extendedLayoutIncludesOpaqueBars { fakeBarFrame.origin.y = 0 }
                let fakeBar = self.toViewControllerFakeBar
                fakeBar.applyConfiguration(config: showConfig)
                fakeBar.frame = fakeBarFrame
                toVC?.view.addSubview(fakeBar)
            }
            UIView.setAnimationsEnabled(true)
        }, completion: { context in
            if context.isCancelled {
                self.removeFakeBars()
                navigationBar.applyConfiguration(config: currentConfig)
                if currentConfig.isHidden != navigationController.isNavigationBarHidden {
                    navigationController.setNavigationBarHidden(currentConfig.isHidden, animated: animated)
                }
            }
        })

        let popInteractionEndBlock: (_ context: UIViewControllerTransitionCoordinatorContext) -> Void = { context in
            if context.isCancelled {
                navigationBar.adjustTintColor(config: currentConfig)
                navigationBar.adjustTitleColor(config: currentConfig, item: currentItem)
            }
        }
        if #available(iOS 10.0, *) {
            navigationController.transitionCoordinator?.notifyWhenInteractionChanges(popInteractionEndBlock)
        } else {
            navigationController.transitionCoordinator?.notifyWhenInteractionEnds(popInteractionEndBlock)
        }
    }

    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        navigationController.navigationBar.applyConfiguration(config: viewController.nk.navigationConfig)
        navigationController.navigationBar.adjustTitleColor(config: viewController.nk.navigationConfig, item: viewController.navigationItem)
        removeFakeBars()
    }

    // MARK: - Helpers

    /// 移除模拟的导航栏
    func removeFakeBars() {
        toViewControllerFakeBar.removeFromSuperview()
        fromViewControllerFakeBar.removeFromSuperview()
    }

    /// 判断是否需要模拟的导航栏
    /// - Parameter currentConfig: 当前配置
    /// - Parameter showConfig: 将要显示的配置
    func needFakeBar(currentConfig: NavigationConfiguration, showConfig: NavigationConfiguration) -> Bool {
        if currentConfig === showConfig { return false }
        let isShadowEqual = (currentConfig.shadowColor == nil && showConfig.shadowColor == nil) || (currentConfig.shadowColor == showConfig.shadowColor)
        let isBackImageEqual = (currentConfig.barBackgroundImage == nil && showConfig.barBackgroundImage == nil)
                                || (currentConfig.barBackgroundImage != nil && currentConfig.barBackgroundImageIdentifier == showConfig.barBackgroundImageIdentifier)
                                || (showConfig.barBackgroundImage != nil && showConfig.barBackgroundImageIdentifier == currentConfig.barBackgroundImageIdentifier)
        return !(currentConfig.isTransparent == showConfig.isTransparent &&
            currentConfig.isTranslucent == showConfig.isTranslucent &&
            currentConfig.isHidden == showConfig.isHidden &&
            currentConfig.barTintColor == showConfig.barTintColor &&
            isShadowEqual &&
            isBackImageEqual)
    }
}
