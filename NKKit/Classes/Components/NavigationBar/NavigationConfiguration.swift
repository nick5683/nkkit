//
//  NavigationConfiguration.swift
//
//  Created by Nick on 2019/11/13.
//

import UIKit

public class NavigationConfiguration: NSCopying {
    /// 全局配置
    public static var defaultConfig: NavigationConfiguration = NavigationConfiguration()
    /// 是否透明的
    public var isTransparent: Bool = false
    /// 是否半透明
    public var isTranslucent: Bool = false
    /// 是否隐藏的
    public var isHidden: Bool = false
    /// 主题色
    public var tintColor: UIColor = .black
    /// 状态栏颜色
    public var statusBarStyle: UIStatusBarStyle = .default
    /// 标题颜色
    public var titleColor: UIColor = .black
    /// 背景色
    public var barTintColor: UIColor = .white
    /// 背景图片，直接设置此属性，identifier为空
    public var barBackgroundImage: UIImage? {
        didSet { barBackgroundImageIdentifier = nil }
    }
    /// 背景图片ID，同样的图片赋值同样的ID，用此属性判定是否一样
    private(set) var barBackgroundImageIdentifier: String?
    /// 返回按钮图片
    public var backImage: UIImage?

    /// 底部线条颜色
    public var shadowColor: UIColor?
    /// 动态进度，0 - 1
    public var dynamicProgress: CGFloat = 0 {
        didSet {
            dynamicProgress = min(1, max(0, dynamicProgress))
            if oldValue == dynamicProgress { return }
            dynamicBlock?(dynamicProgress)
        }
    }
    /// 动态进度改变将会调用block，注意循环引用
    public var dynamicBlock: ((_ progress: CGFloat) -> Void)?
    /// Bar背景是否透明
    public var isBarBackVisible: Bool {
        return !(isTransparent || isHidden)
    }
    
    /// 是否允许侧滑手势返回
    public var isAllowGestureBack: Bool = true
    
    // MARK：- Public
    
    /// 设置背景图片
    /// - Parameter image: 图片
    /// - Parameter identifier: 标识
    public func setbarBackgroundImage(image: UIImage, identifier: String) {
        barBackgroundImage = image
        barBackgroundImageIdentifier = identifier
        if barBackgroundImage == nil { barBackgroundImageIdentifier = nil }
    }
    
    // MARK: - NSCopying
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let config = NavigationConfiguration()
        config.isHidden = isHidden
        config.isTranslucent = isTranslucent
        config.isTransparent = isTransparent
        config.statusBarStyle = statusBarStyle
        config.tintColor = tintColor
        config.titleColor = titleColor
        config.barTintColor = barTintColor
        config.backImage = backImage
        config.barBackgroundImage = barBackgroundImage
        config.barBackgroundImageIdentifier = barBackgroundImageIdentifier
        config.shadowColor = shadowColor
        return config
    }
}
