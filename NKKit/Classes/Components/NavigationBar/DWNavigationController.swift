//
//  DWNavigationController.swift
//
//  Created by Nick on 2019/11/14.
//

import UIKit

open class DWNavigationController: UINavigationController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    var transitioner: NavigationTransitioner!

    // MARK: - Init

    /// 使用之前先调用此方法初始化
    public static func initCustomNavigationBar() {
        UIViewController.initMethodHook()
    }

    // MARK: - UINavigationControllerDelegate

    open func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        transitioner.navigationController(navigationController, willShow: viewController, animated: animated)
    }

    open func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        transitioner.navigationController(navigationController, didShow: viewController, animated: animated)
    }

    // MARK: - UIGestureRecognizerDelegate

    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == interactivePopGestureRecognizer {
            let isAllowGestureBack = topViewController?.nk.navigationConfig.isAllowGestureBack ?? true
            return isAllowGestureBack && viewControllers.count > 1
        }
        return true
    }

    // MARK: - Override

    open override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        interactivePopGestureRecognizer?.delegate = self
        transitioner = NavigationTransitioner()
    }

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
}
