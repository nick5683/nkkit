//
//  UIViewController+NavigationBar.swift
//
//  Created by Nick on 2019/11/15.
//

import UIKit

extension UIViewController {
    
    static func initMethodHook() {
        let method1 = class_getInstanceMethod(self, #selector(getter: preferredStatusBarStyle))
        let method2 = class_getInstanceMethod(self, #selector(getter: nk_preferredStatusBarStyle))
        method_exchangeImplementations(method1!, method2!)
    }
    
    @objc open func initNavigationConfig() {
    }
    
    @objc var nk_preferredStatusBarStyle: UIStatusBarStyle {
        if navigationController is DWNavigationController {
            return nk.navigationConfig.statusBarStyle
        }
        return self.nk_preferredStatusBarStyle
    }
}

private struct AssociatedKeys {
    static var navigationConfig = "navigationConfig"
}

public extension NKFramework where Base: UIViewController {
    var navigationConfig: NavigationConfiguration! {
        set {
            objc_setAssociatedObject(base, &AssociatedKeys.navigationConfig, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            var config = objc_getAssociatedObject(base, &AssociatedKeys.navigationConfig) as? NavigationConfiguration
            if config == nil {
                config = NavigationConfiguration.defaultConfig.copy() as? NavigationConfiguration
                objc_setAssociatedObject(base, &AssociatedKeys.navigationConfig, config, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                base.initNavigationConfig()
                config?.dynamicBlock?(config!.dynamicProgress)
            }
            return config
        }
    }

    // MARK: -

    internal func fakeBarFrameForNavigationBar(navigationBar: UINavigationBar?) -> CGRect? {
        if navigationBar == nil { return nil }
        guard let backgroundView = navigationBar?.nk_backgroundView else { return nil }
        var frame = backgroundView.superview?.convert(backgroundView.frame, to: base.view)
        frame?.origin.x = base.view.bounds.origin.x
        return frame
    }

    // MARK: - Public

    func updateNavigationConfig() {
        guard let navigationController = base.navigationController as? DWNavigationController else { return }
        navigationController.navigationBar.adjustTitleColor(config: navigationConfig, item: navigationController.navigationBar.topItem!)
        navigationController.navigationBar.applyConfiguration(config: navigationConfig)
        base.setNeedsStatusBarAppearanceUpdate()
    }
}
