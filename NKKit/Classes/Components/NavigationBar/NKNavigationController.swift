//
//  NKNavigationController.swift
//  NKKit
//
//  Created by dhcc on 2023/12/23.
//

import UIKit


//定义协议
protocol NK_NavBarDelegate:AnyObject{
    //回调方法
    func navigationShouldPopOnBackButton() -> Bool
}

class NKNavigationController: UINavigationController , UINavigationBarDelegate, UINavigationControllerDelegate{
    public var navBackImage:UIImage?
    weak var navDelegate:NK_NavBarDelegate?
    
    var popDelegate: UIGestureRecognizerDelegate?
    var backBtn = UIButton(frame: CGRect(x: 8, y: 0, width: 30, height: 44))
    
    private var viewTransitionInProgress:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        let backImage = navBackImage?.withRenderingMode(.alwaysOriginal)
        backBtn.setImage(backImage, for: .normal)
        backBtn.setImage(backImage, for: .highlighted)

        backBtn.addTarget(self, action: #selector(backClick), for: .touchUpInside)
                
        navigationBar.addSubview(backBtn)
        
        self.popDelegate = self.interactivePopGestureRecognizer?.delegate
        
        self.delegate = self
        
        navigationBar.setNavBarDefaultBgColor()

    }
    
    override var childForStatusBarStyle: UIViewController?{
        return self.visibleViewController
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: ------------------------
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        if viewTransitionInProgress == false {
            super.pushViewController(viewController, animated: animated)
            viewTransitionInProgress = true
        }
        
        if viewController.navigationItem.leftBarButtonItem == nil && self.viewControllers.count > 1{
            viewController.navigationItem.hidesBackButton = true
            self.backBtn.isHidden = false
        }else{
            self.backBtn.isHidden = true
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        viewTransitionInProgress = false
        if viewController == self.viewControllers.first {
            self.backBtn.isHidden = true
            self.interactivePopGestureRecognizer!.delegate = self.popDelegate
        } else {
            self.interactivePopGestureRecognizer!.delegate = nil
        }
    }
    
    @objc func backClick() {
        
        if (navDelegate != nil) {
            if navDelegate!.navigationShouldPopOnBackButton() {
                self.popViewController(animated: true)
            }
        }else{
            self.popViewController(animated: true)
        }
    }
    //replaceStrToFunc
    
    func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
        
        if (self.viewControllers.count) < (navigationBar.items?.count ?? 0) {
            return true
        }
        
        var shouldPop = true
        if ( navDelegate != nil ) {
            shouldPop = navDelegate!.navigationShouldPopOnBackButton()
        }
        
        if shouldPop {
            DispatchQueue.main.async() {
                self.popViewController(animated: true)
            }
        } else {
            // 取消 pop 后，复原返回按钮的状态
            /*__系统返回按钮会随着返回动画而边淡__*/
            for subView in navigationBar.subviews {
                if subView.alpha < 1.0 {
                    UIView.animate(withDuration: 0.25, animations: {
                        subView.alpha = 1.0
                    })
                }
            }
        }
        return false
    }
    
}
