//
//  UINavigationBar+NavigationBar.swift
//
//  Created by Nick on 2019/11/15.
//

import UIKit

extension UINavigationBar {
    
    var nk_backgroundView: UIView? {
        return self.value(forKey: "_backgroundView") as? UIView
    }
    
    private struct AssociatedKeys {
        static var currentNavigationConfig = "currentNavigationConfig"
    }
    internal var currentNavigationConfig: NavigationConfiguration? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.currentNavigationConfig, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
        get { objc_getAssociatedObject(self, &AssociatedKeys.currentNavigationConfig) as? NavigationConfiguration }
    }
    
    /// 修正主题色
    func adjustTintColor(config: NavigationConfiguration) {
        tintColor = config.tintColor
        var attM = titleTextAttributes
        attM?[NSAttributedString.Key.foregroundColor] = config.titleColor
        titleTextAttributes = attM
    }
    
    /// 修正标题颜色
    func adjustTitleColor(config: NavigationConfiguration, item: UINavigationItem) {
        if item.titleView != nil && !(item.titleView is UILabel) { return }
        var titleLabel = item.titleView as? UILabel
        if titleLabel == nil {
            titleLabel = UILabel()
            item.titleView = titleLabel
        }
        var attM = titleTextAttributes
        if attM == nil { attM = UINavigationBar.appearance().titleTextAttributes }
        if attM == nil { attM = [:] }
        attM?[NSAttributedString.Key.foregroundColor] = config.titleColor
        var title = " "
        if let itemTitle = item.title, itemTitle.count > 0 {
            title = itemTitle
        }
        titleLabel?.attributedText = NSAttributedString(string: title, attributes: attM)
        titleLabel?.sizeToFit()
    }
    
    /// 应用导航栏配置
    func applyConfiguration(config: NavigationConfiguration) {
        adjustTintColor(config: config)
        if config.isTransparent {
            nk_backgroundView?.alpha = 0
            isTranslucent = true
            setBackgroundImage(UIImage(), for: .default)
        } else {
            nk_backgroundView?.alpha = 1
            isTranslucent = config.isTranslucent
            var backImage = config.barBackgroundImage
            if backImage == nil { backImage = UIImage.createImage(color: config.barTintColor) }
            setBackgroundImage(backImage, for: .default)
        }
        shadowImage = UIImage.createImage(color: config.shadowColor ?? .clear)
        currentNavigationConfig = config
    }
}
