//
//  FakeNavigationBar.swift
//
//  Created by Nick on 2020/1/3.
//

import UIKit

class FakeNavigationBar: UIToolbar {
    var lineView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        // Fixed: iOS 12.x 13.x setImage失效了，用view模拟底部线条
        lineView = UIView()
        lineView.layer.zPosition = 100
        addSubview(lineView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let height = 2 / UIScreen.main.scale
        lineView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: height)
    }

    /// 应用导航栏配置
    func applyConfiguration(config: NavigationConfiguration) {
        if config.isTransparent {
            isTranslucent = true
            setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default)
        } else {
            isTranslucent = config.isTranslucent
            var backImage = config.barBackgroundImage
            if backImage == nil { backImage = UIImage.createImage(color: config.barTintColor) }
            setBackgroundImage(backImage, forToolbarPosition: .any, barMetrics: .default)
        }
        setShadowImage(UIImage(), forToolbarPosition: .any)
        lineView.backgroundColor = config.shadowColor
    }
}
