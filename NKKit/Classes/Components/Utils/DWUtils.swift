//
//  DWUtils.swift
//
//  Created by Nick on 2019/12/23.
//

import MapKit
import UIKit

public class DWUtils {
    /// 拨打电话
    /// - Parameter phone: 电话号
    public static func callPhone(_ phone: String) {
        let callBlock = {
            guard let url = URL(string: "telprompt:\(phone)") else { return }
            UIApplication.shared.open(url){ _ in    }
        }
        if #available(iOS 10.0, *) {
            callBlock()
        } else {
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc?.nk.showConfirmAlert(title: nil, message: phone, confirmTitle: "呼叫", block: {
                callBlock()
            })
        }
    }
    
    /// 发送邮件
    /// - Parameter email: 邮箱账号
    public static func openEmail(_ email: String) {
        let callBlock = {
            guard let url = URL(string: "mailto:\(email)") else { return }
            UIApplication.shared.open(url){ _ in    }
        }
        if #available(iOS 10.0, *) {
            callBlock()
        } else {
            let vc = UIApplication.shared.keyWindow?.rootViewController
            vc?.nk.showConfirmAlert(title: nil, message: email, confirmTitle: "呼叫", block: {
                callBlock()
            })
        }
    }
    ///  打开本机地图，需要设置LSApplicationQueriesSchemes
    ///  百度地图 = baidumap，高德地图 = iosamap
    /// - Parameters:
    ///   - lat: 纬度
    ///   - lng: 经度
    ///   - address: 地址
    public static func openLocalMap(lat: Float, lng: Float, address: String? = nil) {
        let isOpenBaiduMap = UIApplication.shared.canOpenURL(URL(string: "baidumap://")!)
        let isOpenGaoDeMap = UIApplication.shared.canOpenURL(URL(string: "iosamap://")!)
        var actions: [String] = []
        actions.append("取消")
        actions.append("苹果地图")
        if isOpenBaiduMap { actions.append("百度地图") }
        if isOpenGaoDeMap { actions.append("高德地图") }

        let vc = UIApplication.shared.keyWindow?.rootViewController
        vc?.nk.showAlert(title: "地图导航", message: nil, style: .actionSheet, actions: actions, cancelIndex: 0, destructiveIndex: -1, block: { index in
            if index == 1 {
                let loc = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))
                let currentLocation = MKMapItem.forCurrentLocation()
                let toLocation = MKMapItem(placemark: MKPlacemark(coordinate: loc, addressDictionary: nil))
                toLocation.name = address
                MKMapItem.openMaps(with: [currentLocation, toLocation], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsShowsTrafficKey: true])
            } else if isOpenBaiduMap && index == 2 {
                let charSet = NSMutableCharacterSet()
                charSet.formUnion(with: CharacterSet.urlQueryAllowed)
                charSet.addCharacters(in: "#")
                let urlString = "baidumap://map/direction?origin={{我的位置}}&destination=latlng:\(lat),\(lng)|name=\(address ?? "")&mode=driving&coord_type=gcj02".addingPercentEncoding(withAllowedCharacters: charSet as CharacterSet)
                guard let url = URL(string: urlString ?? "") else { return }
                UIApplication.shared.open(url){ _ in    }

            } else if isOpenGaoDeMap && (index == 2 || index == 3) {
                let charSet = NSMutableCharacterSet()
                charSet.formUnion(with: CharacterSet.urlQueryAllowed)
                charSet.addCharacters(in: "#")
                let urlString = "iosamap://navi?sourceApplication=\"\"&backScheme=\"\"&lat=\(lat)&lon=\(lng)&dev=0&style=2".addingPercentEncoding(withAllowedCharacters: charSet as CharacterSet)
                guard let url = URL(string: urlString ?? "") else { return }
                UIApplication.shared.open(url){ _ in    }
            }
        })
    }

    /// BD09地址转GCJ02地址
    /// - Parameter location: 地址经纬度
    public static func bd09ToGcj02(location: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let x = location.longitude - 0.0065
        let y = location.latitude - 0.006
        let x_pi = Double.pi * 3000 / 180
        let z = sqrt(x * x + y * y) - 0.00002 * sin(y * x_pi)
        let theta = atan2(y, x) - 0.000003 * cos(x * x_pi)
        let tempLng = z * cos(theta)
        let tempLat = z * sin(theta)
        return CLLocationCoordinate2D(latitude: tempLat, longitude: tempLng)
    }

    /// 打开QQ，与指定QQ聊天，需要设置LSApplicationQueriesSchemes(mqq)
    /// - Parameter qq: qq
    public static func contactQQ(_ qq: String) {
        if UIApplication.shared.canOpenURL(URL(string: "mqq://")!) {
            let urlString = "mqq://im/chat?chat_type=wpa&uin=\(qq)&version=1&src_type=web"
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url){ _ in    }
            }
        }
    }
}
