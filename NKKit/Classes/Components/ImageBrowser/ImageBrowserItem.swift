//
//  ImageBrowserItem.swift
//
//  Created by Nick on 2019/12/10.
//

import UIKit

public class ImageBrowserItem {
    /// 图片所属视图
    weak var owner: UIView?
    /// 图片URL
    var imageURL: URL?
    /// 图片
    var image: UIImage?
    /// 图片大小
    var imageSize: CGSize = .zero

    public init(_ image: UIImage, owner: UIView? = nil) {
        self.image = image
        self.owner = owner
        imageSize = image.size
    }

    public init(_ imageURL: URL, owner: UIView? = nil) {
        self.imageURL = imageURL
        self.owner = owner
    }
}
