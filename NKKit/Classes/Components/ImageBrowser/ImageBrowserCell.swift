//
//  ImageBrowserCell.swift
//
//  Created by Nick on 2019/12/10.
//

import UIKit

public class ImageBrowserCell: UIView, UIScrollViewDelegate {
    var index: Int = -1
    private(set) var imageView: UIImageView!
    private(set) var imageContainerView: UIScrollView!
    private var indicatorView: UIActivityIndicatorView!

    // MARK: -

    func prepareForReuse() {
        indicatorView.stopAnimating()
        imageContainerView.zoomScale = 1
    }

    func willLoadImage() {
        indicatorView.startAnimating()
    }

    func didLoadImage() {
        indicatorView.stopAnimating()
    }

    // MARK: - UIScrollViewDelegate

    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let width = scrollView.bounds.width
        let height = scrollView.bounds.height
        let offsetX = width > scrollView.contentSize.width ? (width - scrollView.contentSize.width) * 0.5 : 0
        let offsetY = (height > scrollView.contentSize.height) ? (height - scrollView.contentSize.height) * 0.5 : 0
        imageView.center = CGPoint(x: scrollView.contentSize.width * 0.5 + offsetX, y: scrollView.contentSize.height * 0.5 + offsetY)
    }

    // MARK: - Override

    override public func layoutSubviews() {
        super.layoutSubviews()
        indicatorView.sizeToFit()
        indicatorView.center = CGPoint(x: bounds.width * 0.5, y: bounds.height * 0.5)
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initSubviews() {
        imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill

        imageContainerView = UIScrollView()
        imageContainerView.delegate = self
        imageContainerView.maximumZoomScale = 3
        imageContainerView.minimumZoomScale = 1
        imageContainerView.showsVerticalScrollIndicator = false
        imageContainerView.showsHorizontalScrollIndicator = false
        if #available(iOS 11.0, *) {
            imageContainerView.contentInsetAdjustmentBehavior = .never
        }

        indicatorView = UIActivityIndicatorView(style: .white)
        indicatorView.hidesWhenStopped = true

        addSubview(imageContainerView)
        imageContainerView.addSubview(imageView)
        addSubview(indicatorView)
    }
}
