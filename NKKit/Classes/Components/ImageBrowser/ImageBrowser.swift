//
//  ImageBrowser.swift
//
//  Created by Nick on 2019/12/10.
//

import Kingfisher
import Photos
import UIKit

public class ImageBrowser: UIView, UIScrollViewDelegate {
    /// 图片分页，左右间距
    var spacingLR: CGFloat = 10
    public private(set) var images: [ImageBrowserItem] {
        didSet {
            currentPage = images.count > 0 ? 0 : -1
            pageControl.isHidden = images.count < 2
            pageControl.numberOfPages = images.count
        }
    }

    private(set) var currentPage: Int = -1 {
        didSet {
            pageControl.currentPage = currentPage
        }
    }

    var scrollView: UIScrollView = UIScrollView()
    lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.insertSubview(view, at: 0)
        return view
    }()

    var pageControl: UIPageControl = UIPageControl()
    var visibleCells: [ImageBrowserCell] = []
    var reusableCells: [ImageBrowserCell] = []
    var viewController: UIViewController?
    var containerWindow: UIWindow?

    @discardableResult
    public static func show(_ images: [ImageBrowserItem], currentPage: Int? = nil, triggerView: UIView? = nil) -> ImageBrowser {
        let window = UIWindow(frame: UIScreen.main.bounds)
        if #available(iOS 13.0, *) {
            window.windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        }
        window.backgroundColor = UIColor.clear
        window.rootViewController = ImageBrowserViewController()
        window.windowLevel = UIWindow.Level(999)
        window.makeKeyAndVisible()

        let browser = ImageBrowser(images)
        // didSet方法不在init中调用，触发一次didSet方法
        browser.images = images
        browser.viewController = window.rootViewController
        browser.containerWindow = window
        browser.show(to: window.rootViewController!.view, currentPage: currentPage, triggerView: triggerView)
        return browser
    }

    public func show(to view: UIView, currentPage: Int? = nil, triggerView: UIView?) {
        frame = view.bounds
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(self)
        setNeedsLayout()
        layoutIfNeeded()
        if currentPage != nil {
            setCurrentPage(currentPage!, animated: false)
        } else if let triggerView = triggerView {
            for i in 0 ..< images.count {
                if images[i].owner == triggerView {
                    setCurrentPage(i, animated: false)
                    break
                }
            }
        }
        if self.currentPage < 0 || self.currentPage >= images.count {
            self.currentPage = 0
        }

        if let triggerView = triggerView {
            if let cell = cellForItem(at: self.currentPage) {
                cell.imageView.frame = cell.imageContainerView.convert(triggerView.frame, from: triggerView.superview)
                backgroundView.alpha = 0
                UIView.animate(withDuration: 0.25) {
                    self.backgroundView.alpha = 1
                    self.layoutCellContent(cell)
                }
            }
        } else {
            alpha = 0
            backgroundView.alpha = 0
            UIView.animate(withDuration: 0.25) {
                self.backgroundView.alpha = 1
                self.alpha = 1
            }
        }
    }

    public func dismiss() {
        let item = images[currentPage]
        if let owner = item.owner, let ownerSuperView = owner.superview, let cell = cellForItem(at: currentPage) {
            UIView.animate(withDuration: 0.25, animations: {
                self.backgroundView.alpha = 0
                self.containerWindow?.alpha = 0
                cell.imageView.frame = cell.imageContainerView.convert(owner.frame, from: ownerSuperView)
            }) { _ in
                self.containerWindow?.removeFromSuperview()
                self.removeFromSuperview()
            }
        } else {
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha = 0
                self.containerWindow?.alpha = 0
            }) { _ in
                self.removeFromSuperview()
                self.containerWindow?.removeFromSuperview()
            }
        }
    }

    public func setCurrentPage(_ page: Int, animated: Bool) {
        currentPage = page
        scrollView.scrollRectToVisible(CGRect(x: CGFloat(currentPage) * scrollView.bounds.width, y: 0, width: scrollView.bounds.width, height: scrollView.bounds.height), animated: animated)
    }

    // MARK: - Action

    @objc func onSingleTap() {
        dismiss()
    }

    @objc func onDoubleTap(_ gesture: UITapGestureRecognizer) {
        guard let cell = cellForItem(at: self.currentPage) else { return }
        if cell.imageContainerView.zoomScale > CGFloat(1) {
            cell.imageContainerView.setZoomScale(1, animated: true)
        } else {
            let touchPoint = gesture.location(in: cell.imageView)
            let newZoomScale = cell.imageContainerView.maximumZoomScale
            let xSize = scrollView.bounds.width / newZoomScale
            let ySize = scrollView.bounds.height / newZoomScale
            cell.imageContainerView.zoom(to: CGRect(x: touchPoint.x - xSize / 2, y: touchPoint.y - ySize / 2, width: xSize, height: ySize), animated: true)
        }
    }

    @objc func onLongPressed(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state != .began { return }

        let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: "保存到相册", style: .default, handler: { _ in
            PHPhotoLibrary.requestAuthorization { status in
                if status != .authorized {
                    self.showAlert("无访问相册权限")
                    return
                }
                if let image = self.images[self.currentPage].image {
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAsset(from: image)
                    }) { success, error in
                        if success {
                            DispatchQueue.main.async {
                                self.showAlert("保存成功")
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.showAlert(error?.localizedDescription ?? "未知错误")
                            }
                        }
                    }
                }
            }
        }))
        alertVC.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        let viewController = self.viewController ?? UIApplication.shared.keyWindow?.rootViewController
        viewController?.present(alertVC, animated: true, completion: nil)
    }

    // MARK: - UIScrollViewDelegate

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentPage = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        layoutCells()
    }

    // MARK: - Layout

    /// 布局应该显示的Cell
    func layoutCell(minX: CGFloat, maxX: CGFloat) {
        if images.count == 0 { return }
        if visibleCells.count == 0 {
            layoutNewCellOnRight(minX: minX, index: images.count - 1)
        }

        while let lastCell = visibleCells.last,
            lastCell.frame.maxX < maxX && lastCell.index + 1 < images.count {
            layoutNewCellOnRight(minX: minX, index: lastCell.index)
        }
        while let firstCell = visibleCells.first,
            firstCell.frame.minX > minX && firstCell.index - 1 >= 0 {
            layoutNewCellOnLeft(minX: minX, index: firstCell.index)
        }

        while let lastCell = visibleCells.last,
            lastCell.frame.maxX > maxX {
            lastCell.removeFromSuperview()
            visibleCells.removeAll { $0 == lastCell }
            reusableCells.append(lastCell)
        }

        while let firstCell = visibleCells.first,
            firstCell.frame.minX < minX {
            firstCell.removeFromSuperview()
            visibleCells.removeAll { $0 == firstCell }
            reusableCells.append(firstCell)
        }
    }

    func layoutNewCellOnRight(minX: CGFloat, index: Int) {
        let nextIndex = (index + 1) % images.count
        let cell = dequeueReusableCell()
        cell.index = nextIndex
        scrollView.addSubview(cell)
        visibleCells.append(cell)
        cell.frame = CGRect(x: CGFloat(nextIndex) * scrollView.bounds.width, y: 0, width: scrollView.bounds.width, height: scrollView.bounds.height)
        layoutCellContent(cell)
    }

    func layoutNewCellOnLeft(minX: CGFloat, index: Int) {
        let nextIndex = (index - 1) % images.count
        let cell = dequeueReusableCell()
        cell.index = nextIndex
        scrollView.addSubview(cell)
        visibleCells.insert(cell, at: 0)
        cell.frame = CGRect(x: CGFloat(nextIndex) * scrollView.bounds.width, y: 0, width: scrollView.bounds.width, height: scrollView.bounds.height)
        layoutCellContent(cell)
    }

    // 计算cell内容布局
    func layoutCellContent(_ cell: ImageBrowserCell) {
        cell.imageContainerView.frame = CGRect(x: spacingLR, y: 0, width: cell.bounds.width - spacingLR * 2, height: cell.bounds.height)
        let calculateImageSize: (ImageBrowserCell) -> Void = { cell in
            let item = self.images[cell.index]
            var x: CGFloat = 0
            var y: CGFloat = 0
            var width: CGFloat = item.imageSize.width
            var height: CGFloat = item.imageSize.height
            if item.imageSize.width > self.bounds.width {
                width = self.bounds.width
                height = item.imageSize.height / item.imageSize.width * width
            } else {
                x = (self.bounds.width - width) * 0.5
            }
            if height < self.bounds.height {
                y = self.bounds.height * 0.5 - height * 0.5
            }
            cell.imageView.frame = CGRect(x: x, y: y, width: width, height: height)
            cell.imageContainerView.contentSize = CGSize(width: width, height: max(height, self.bounds.height))
        }
        let index = cell.index
        let item = images[index]
        if let image = item.image {
            cell.imageView.image = image
            calculateImageSize(cell)
            return
        }
        if let imageURL = item.imageURL {
            cell.willLoadImage()
            cell.imageView.kf.setImage(with: imageURL) { r in
                switch r {
                case let .success(result):
                    if cell.index == index, self.images[index].imageSize == .zero {
                        cell.didLoadImage()
                        self.images[index].image = result.image
                        self.images[index].imageSize = result.image.size
                        calculateImageSize(cell)
                    }
                default: break
                }
            }
            calculateImageSize(cell)
        }
    }

    func layoutCells() {
        let minX = CGFloat(currentPage - 1) * scrollView.bounds.width
        let maxX = CGFloat(currentPage + 2) * scrollView.bounds.width
        layoutCell(minX: minX, maxX: maxX)
    }

    // MARK: - Helpers

    func dequeueReusableCell() -> ImageBrowserCell {
        var cell: ImageBrowserCell?
        if reusableCells.count > 0 {
            cell = reusableCells[0]
            reusableCells.remove(at: 0)
            cell?.prepareForReuse()
        }
        if cell == nil {
            cell = ImageBrowserCell()
        }
        return cell!
    }

    func cellForItem(at index: Int) -> ImageBrowserCell? {
        return visibleCells.first { $0.index == index }
    }

    func showAlert(_ message: String) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "确定", style: .default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alertVC, animated: true, completion: nil)
    }

    // MARK: - Override

    public override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.frame = CGRect(x: -spacingLR, y: 0, width: bounds.width + spacingLR * 2, height: bounds.height)
        scrollView.contentSize = CGSize(width: scrollView.bounds.width * CGFloat(images.count), height: scrollView.bounds.height)
        pageControl.sizeToFit()
        pageControl.frame.size.height = 30
        pageControl.center = CGPoint(x: bounds.width * 0.5, y: bounds.height * 0.9)
        layoutCells()
    }

    // MARK: - Init

    public init(_ images: [ImageBrowserItem]) {
        self.images = images
        super.init(frame: .zero)
        initSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initSubviews() {
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        addSubview(scrollView)
        addSubview(pageControl)

        let singleTap = UITapGestureRecognizer(target: self, action: #selector(onSingleTap))
        addGestureRecognizer(singleTap)

        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(onDoubleTap))
        doubleTap.numberOfTapsRequired = 2
        addGestureRecognizer(doubleTap)

        singleTap.require(toFail: doubleTap)

        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(onLongPressed))
        addGestureRecognizer(longPress)
    }
}

class ImageBrowserViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
