//
//  UIViewController+Data.swift
//
//  Created by Nick on 2019/11/12.

import MJRefresh
import UIKit

extension UIViewController {
    /// 头部刷新控件调用此方法
    @objc open func loadNewData() {
        loadData(isLoadMore: false)
    }

    /// 尾部刷新控件调用此方法
    @objc open func loadMoreData() {
        loadData(isLoadMore: true)
    }

    /// 加载新数据和加载更多数据行为一致的话，最好直接实现此方法
    @objc open func loadData(isLoadMore: Bool) {
    }
}

public extension NKFramework where Base: UIViewController {
    /// 设置头部刷新控件
    func setRefreshHeader(view: UIScrollView, target: Any? = nil, action: Selector? = nil) {
        if view.mj_header != nil { return }
        let refreshingTarget = target ?? base
        let refreshingAction = action ?? #selector(UIViewController.loadNewData)
        let header = MJRefreshNormalHeader(refreshingTarget: refreshingTarget, refreshingAction: refreshingAction)
        header.isAutomaticallyChangeAlpha = true
        header.lastUpdatedTimeLabel?.isHidden = true
        view.mj_header = header
    }

    /// 设置尾部刷新控件
    func setRefreshFooter(view: UIScrollView, target: Any? = nil, action: Selector? = nil) {
        let refreshingTarget = target ?? base
        let refreshingAction = action ?? #selector(UIViewController.loadMoreData)
        let footer = MJRefreshAutoNormalFooter(refreshingTarget: refreshingTarget, refreshingAction: refreshingAction)
        footer.setTitle("加载更多", for: .idle)
        footer.setTitle("加载中...", for: .refreshing)
        footer.setTitle("- END -", for: .noMoreData)
        footer.stateLabel?.font = UIFont.systemFont(ofSize: 13)
        footer.stateLabel?.textColor = UIColor.lightGray
        footer.triggerAutomaticallyRefreshPercent = 0.1
        footer.isHidden = true
        view.mj_footer = footer
    }

    /// 调起头部刷新控件刷新
    func startRefreshing(view: UIScrollView) {
        setRefreshHeader(view: view)
        if view.mj_header!.isRefreshing { return }
        view.mj_header?.beginRefreshing()
    }

    /// 结束头部刷新控件刷新状态
    func endRefreshHeader(view: UIScrollView) {
        view.mj_header?.endRefreshing()
    }

    /// 结束尾部刷新控件刷新状态
    func endRefreshFooter(view: UIScrollView) {
        endRefreshFooter(view: view, isNoMoreData: false)
    }

    /// 结束尾部刷新控件刷新状态
    func endRefreshFooter(view: UIScrollView, isNoMoreData: Bool) {
        if isNoMoreData {
            view.mj_footer?.endRefreshingWithNoMoreData()
        } else {
            view.mj_footer?.endRefreshing()
        }
    }
}
