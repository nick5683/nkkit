//
//  UIViewController+NavigationBarHook.swift
//
//  Created by Nick on 2019/11/12.
//

import UIKit

extension UIViewController {
    
    public class func initNavigationBarHook() {
        let selectorArray = [#selector(UIViewController.viewWillDisappear(_:))]
        let newSelectorArray = [#selector(UIViewController.nk_viewWillDisappear(_:))]
        for i in 0..<selectorArray.count {
            let oldSelector = class_getInstanceMethod(self, selectorArray[i])
            let newSelector = class_getInstanceMethod(self, newSelectorArray[i])
            method_exchangeImplementations(oldSelector!, newSelector!)
        }
    }
    
    @objc func nk_viewWillDisappear(_ animated: Bool) {
        nk_viewWillDisappear(animated)
        if (navigationController?.viewControllers.count ?? 0) > 0 && navigationItem.backBarButtonItem == nil {
            // FIXED: iPhone 6p设置空串会显示返回，设置空格没问题
            navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        }
    }
}
