# NKKit

[![CI Status](https://img.shields.io/travis/Nick5683/NKKit.svg?style=flat)](https://travis-ci.org/Nick5683/NKKit)
[![Version](https://img.shields.io/cocoapods/v/NKKit.svg?style=flat)](https://cocoapods.org/pods/NKKit)
[![License](https://img.shields.io/cocoapods/l/NKKit.svg?style=flat)](https://cocoapods.org/pods/NKKit)
[![Platform](https://img.shields.io/cocoapods/p/NKKit.svg?style=flat)](https://cocoapods.org/pods/NKKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NKKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NKKit'  # 全部导入

#  pod 'NKKit', :subspecs => ['Base', 'Categories', 'Views', 'IBAnimatable', 'NavigationBar', 'ProgressHUD', 'OverlayController', 'WebViewController', 'DataAssistant', 'SegmentedView', 'MediaPicker', 'ImageBrowser', 'BadgeView', 'ImageGridView', 'Utils'], :path => '../'

```

## Author

Nick5683, 157132026@qq.com

## License

NKKit is available under the MIT license. See the LICENSE file for more info.
