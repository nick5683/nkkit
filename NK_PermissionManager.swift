//
//  NK_PermissionManager.swift
//  MyApp
//
//  Created by iMac on 2021/11/23.
//

import UIKit

  
 
enum AuthType: String {
    case Location  = "Location" //定位
    case Camera = "Camera"  //相机
    case Library = "Library" //媒体库，相册
    case Audio = "Audio"    //语音。麦克风
    case Health = "Health" //健康
    case Calendar = "Calendar"//日历
    case Speech = "SpeechRecognition" //语音识别
    case BlueTooth = "BlueTooth" //蓝牙
    case Contacts = "Contacts" //通讯录
    case Notes = "Notes"
    case Reminders = "Reminders"
    case None = ""
}


typealias AuthClouser = ((Bool)->())
public class NK_PermissionManager: NSObject {
    
    
    static let shared = NK_PermissionManager()
    private  override init() {   }
    
    #if canImport(CoreBluetooth)
    private lazy var blueTooth:CBCentralManager = {
        let bluetooth = CBCentralManager()
        bluetooth.delegate = self
        return bluetooth
    }()
    #endif
    
    #if canImport(CoreMotion)
    /// 防止获取无效 计步器
    private lazy var cmPedometer:CMPedometer = {
        let dometer = CMPedometer()
        return dometer
    }()
    #endif
    
    #if canImport(CoreLocation)

    /// 定义私有全局变量,解决在iOS 13 定位权限弹框自动消失的问题
    private lazy var locationAuthManager:CLLocationManager = {
        let locationAuth = CLLocationManager()
        return locationAuth
    }()
    #endif
    
    
    //MARK: -----------------
    /**
     系统设置
     - parameters: urlString 可以为系统,也可以为微信:weixin://、QQ:mqq://
     - parameters: action 结果闭包
     */
    class func openSettings(urlStr :String?, clouser: @escaping AuthClouser) {
        
        var url: URL
        if (urlStr != nil) && urlStr?.count ?? 0 > 0 {
            url = URL(string: urlStr!)!
        }else{
            url = URL(string: UIApplication.openSettingsURLString)!
        }
        
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (result) in
                clouser(result)
            }
        }else{
            clouser(false)
        }
    }
}

#if canImport(Alamofire)
import Alamofire
extension NK_PermissionManager {
    /**
     联网权限
     - parameters: action 权限结果闭包
     */
    class func authNetwork(_ clouser:  @escaping AuthClouser) {
        
        let reachabilityManager = NetworkReachabilityManager(host: "www.baidu.com")
        switch reachabilityManager?.status {
        case .reachable(.cellular):
            clouser(true)
        case .reachable(.ethernetOrWiFi):
            clouser(true)
        case .none:
            clouser(false)
        case .notReachable:
            clouser(false)
        case .unknown:
            clouser(false)
        }
    }
}
#endif

#if canImport(MediaPlayer)
import MediaPlayer
/// 媒体资料库/Apple Music
extension NK_PermissionManager {

    /**
     媒体资料库/Apple Music权限
     - parameters: action 权限结果闭包
     */
    class func authMediaPlayerService(_ clouser: @escaping AuthClouser) {
        let authStatus = MPMediaLibrary.authorizationStatus()
        switch authStatus {
            /// 未作出选择
        case .notDetermined:
            MPMediaLibrary.requestAuthorization { (status) in
                if status == .authorized{
                    DispatchQueue.main.async {
                        clouser(true)
                    }
                }else{
                    DispatchQueue.main.async {
                        clouser(false)
                    }
                }
            }
            /// 用户明确拒绝此应用程序的授权，或在设置中禁用该服务。
        case .denied:
            clouser(false)
            /// 该应用程序未被授权使用该服务。由于用户无法改变对该服务的主动限制。此状态，并且个人可能没有拒绝授权。
        case .restricted:
            clouser(false)
            /// 已授权
        case .authorized:
            clouser(true)
            /// 扩展以后可能有的状态,做保护措施
        @unknown default:
            clouser(false)
        }
    }
}

#if canImport(AVFoundation)
extension NK_PermissionManager {
    /**
     相机权限
     - parameters: action 权限结果闭包
     */
    class func authCamera(_ clouser:  @escaping AuthClouser) {
        let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch authStatus {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { (result) in
                if result{
                    DispatchQueue.main.async {
                        clouser(true)
                    }
                }else{
                    DispatchQueue.main.async {
                        clouser(false)
                    }
                }
            }
        case .denied:
            clouser(false)
        case .restricted:
            clouser(false)
        case .authorized:
            clouser(true)
        @unknown default:
            clouser(false)
        }
    }
    
}

#if canImport(Photos)
import Photos
extension NK_PermissionManager {
    /**
     相册权限
     - parameters: action 权限结果闭包
     */
    class func authPhotoLib(_ clouser: @escaping AuthClouser) {
        let authStatus = PHPhotoLibrary.authorizationStatus()
        switch authStatus {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { (status) in
                if status == .authorized{
                    clouser(true)
                }else{
                    clouser(false)
                }
            }
        case .denied:
            clouser(false)
        case .restricted:
            clouser(false)
        case .authorized:
            clouser(true)
        case .limited:
            clouser(false)
        @unknown default:
            clouser(false)
        }
    }
}
#endif

#if canImport(AVFAudio)
import AVFAudio
extension NK_PermissionManager {
    /**
     麦克风权限
     - parameters: action 权限结果闭包
     */
    class func authMicrophone(_ clouser:  @escaping AuthClouser) {
        let authStatus = AVAudioSession.sharedInstance().recordPermission
        switch authStatus {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission { (result) in
                clouser(result)
            }
        case .denied:
            clouser(false)
        case .granted:
            clouser(true)
        @unknown default:
            clouser(false)
        }
    }
    
}
#endif

#if canImport(UserNotifications)
import UserNotifications
extension NK_PermissionManager {

    /**
     推送权限
     - parameters: action 权限结果闭包
     */
    class func authNotification(_ clouser:  @escaping AuthClouser){
        UNUserNotificationCenter.current().getNotificationSettings(){ (setttings) in
            switch setttings.authorizationStatus {
            case .notDetermined:
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .carPlay, .sound]) { (result, error) in
                    if result{
                        DispatchQueue.main.async {
                            clouser(true)
                        }
                    }else{
                        DispatchQueue.main.async {
                            clouser(false)
                        }
                    }
                }
            case .denied:
                clouser(false)
            case .authorized:
                clouser(true)
            case .provisional:
                clouser(true)
            case .ephemeral:
                clouser(false)
            @unknown default:
                clouser(false)
            }
        }
    }
    
    
}
#endif

#if canImport(Contacts)
import Contacts
extension NK_PermissionManager {
    /**
     通讯录权限
     - parameters: action 权限结果闭包
     */
    class func authContacts(_ clouser:  @escaping AuthClouser){
        let authStatus = CNContactStore.authorizationStatus(for: .contacts)
        switch authStatus {
        case .notDetermined:
            CNContactStore().requestAccess(for: .contacts) { (result, error) in
                clouser(result)
            }
        case .restricted:
            clouser(false)
        case .denied:
            clouser(false)
        case .authorized:
            clouser(true)
        @unknown default:
            clouser(false)
        }
    }
    
}

#if canImport(Intents)
import Intents
/// Siri权限
extension NK_PermissionManager {
    /**
     Siri 权限
     - parameters: action 权限结果闭包
     */
    class func authSiri(_ clouser:  @escaping AuthClouser){
        let authStatus = INPreferences.siriAuthorizationStatus()
        switch authStatus {
        case .notDetermined:
            INPreferences.requestSiriAuthorization { (status) in
                if status == .authorized{
                    DispatchQueue.main.async {
                        clouser(true)
                    }
                }else{
                    DispatchQueue.main.async {
                        clouser(false)
                    }
                }
            }
        case .restricted:
            clouser(false)
        case .denied:
            clouser(false)
        case .authorized:
            clouser(true)
        @unknown default:
            clouser(false)
        }
    }
    
}
#endif

#if canImport(Speech)
/// 语音转文字权限
import Speech
extension NK_PermissionManager {
    /**
     语音转文字权限
     - parameters: action 权限结果闭包
     */
    class func authSpeechRecognition(_ clouser:  @escaping AuthClouser){
        let authStatus = SFSpeechRecognizer.authorizationStatus()
        switch authStatus {
        case .notDetermined:
            SFSpeechRecognizer.requestAuthorization { (status) in
                if status == .authorized{
                    DispatchQueue.main.async {
                        clouser(true)
                    }
                }else{
                    DispatchQueue.main.async {
                        clouser(false)
                    }
                }
            }
        case .restricted:
            clouser(false)
        case .denied:
            clouser(false)
        case .authorized:
            clouser(true)
        @unknown default:
            clouser(false)
        }
    }
    
}

#endif

#if canImport(EventKit)
/// 日历、提醒事项
import EventKit
extension NK_PermissionManager {

    /**
     日历
     - parameters: action 权限结果闭包
     */
    class func authCalendarEvent(_ clouser:  @escaping AuthClouser){
        
        //        https://www.jianshu.com/p/158db55924ff
        let authStatus = EKEventStore.authorizationStatus(for: .event)
        switch authStatus {
        case .notDetermined:
            EKEventStore().requestAccess(to: .event) { (result, error) in
                if result{
                    DispatchQueue.main.async {
                        clouser(true)
                    }
                }else{
                    DispatchQueue.main.async {
                        clouser(false)
                    }
                }
            }
        case .restricted:
            clouser(false)
        case .denied:
            clouser(false)
        case .authorized:
            clouser(true)
        @unknown default:
            clouser(false)
        }
    }
    
    /**
     提醒事项
     - parameters: action 权限结果闭包
     */
    class func authReminders(_ clouser:  @escaping AuthClouser){
        let authStatus = EKEventStore.authorizationStatus(for: .reminder)
        switch authStatus {
        case .notDetermined:
            EKEventStore().requestAccess(to: .reminder) { (result, error) in
                if result{
                    DispatchQueue.main.async {
                        clouser(true)
                    }
                }else{
                    DispatchQueue.main.async {
                        clouser(false)
                    }
                }
            }
        case .restricted:
            clouser(false)
        case .denied:
            clouser(false)
        case .authorized:
            clouser(true)
        @unknown default:
            clouser(false)
        }
    }
}
#endif


#if canImport(LocalAuthentication)
/// Face、TouchID
import LocalAuthentication
extension NK_PermissionManager {
    /**
     FaceID或者TouchID 认证
     - parameters: action 权限结果闭包
     */
    class func authFaceOrTouchID(_ clouser:  @escaping ((Bool,Error)->())) {
        let context = LAContext()
        var error: NSError?
        let result = context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error)
        if result {
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "认证") { (res, authError) in
                clouser(res, authError)
            }
        }else{
            print("不可以使用")
        }
    }
}
#endif

#if canImport(HealthKit)
import HealthKit
extension NK_PermissionManager {
    /**
     健康  (写:体能训练、iOS13 听力图 读: 健身记录、体能训练、iOS13 听力图)
     - parameters: action 权限结果闭包
     */
    class func authHealth(_ clouser:  @escaping AuthClouser){
        if HKHealthStore.isHealthDataAvailable(){
            let authStatus = HKHealthStore().authorizationStatus(for: .workoutType())
            switch authStatus {
            case .notDetermined:
                if #available(iOS 13.0, *) {
                    HKHealthStore().requestAuthorization(toShare: [.audiogramSampleType(), .workoutType()], read: [.activitySummaryType(), .workoutType(), .audiogramSampleType()]) { (result, error) in
                        clouser(result)
                    }
                } else {
                    HKHealthStore().requestAuthorization(toShare: [.workoutType()], read: [.activitySummaryType(), .workoutType()]) { (result, error) in
                        clouser(result)
                    }
                }
            case .sharingDenied:
                clouser(false)
            case .sharingAuthorized:
                clouser(true)
            @unknown default:
                clouser(false)
            }
        }else{
            clouser(false)
        }
    }
    
}
#endif

#if canImport(HomeKit)
import HomeKit
extension NK_PermissionManager {

    /**
     家庭、住宅数据
     - parameters: action 权限结果闭包
     */
    class func authHomeKit(_ clouser:  @escaping AuthClouser) {
        if #available(iOS 13.0, *) {
            switch HMHomeManager().authorizationStatus {
            case .authorized:
                clouser(true)
            case .determined:
                clouser(false)
            case .restricted:
                clouser(false)
            default:
                clouser(false)
            }
        } else {
            if (HMHomeManager().primaryHome != nil) {
                clouser(true)
            }else{
                clouser(false)
            }
        }
    }
}
#endif


#if canImport(CoreLocation)
import CoreLocation
extension NK_PermissionManager {
    
    /**
     定位权限
     - parameters: action 权限结果闭包(有无权限,是否第一次请求权限)
     */
    func authLocation(_ clouser:  @escaping ((Bool,Bool)->())) {
        let authStatus = CLLocationManager.authorizationStatus()
        switch authStatus {
        case .notDetermined:
            //由于IOS8中定位的授权机制改变 需要进行手动授权
            locationAuthManager.requestAlwaysAuthorization()
            locationAuthManager.requestWhenInUseAuthorization()
            let status = CLLocationManager.authorizationStatus()
            if  status == .authorizedAlways || status == .authorizedWhenInUse {
                DispatchQueue.main.async {
                    clouser(true && CLLocationManager.locationServicesEnabled(), true)
                }
            }else{
                DispatchQueue.main.async {
                    clouser(false, true)
                }
            }
        case .restricted:
            clouser(false, false)
        case .denied:
            clouser(false, false)
        case .authorizedAlways:
            clouser(true && CLLocationManager.locationServicesEnabled(), false)
        case .authorizedWhenInUse:
            clouser(true && CLLocationManager.locationServicesEnabled(), false)
        @unknown default:
            clouser(false, false)
        }
    }
    
}
#endif


#if canImport(CoreMotion)
import CoreMotion
extension NK_PermissionManager {
    /**
     运动与健身
     - parameters: action 权限结果闭包
     */
    func authCMPedometer(_ clouser:  @escaping AuthClouser){
        cmPedometer.queryPedometerData(from: Date(), to: Date()) { (pedometerData, error) in
            if pedometerData?.numberOfSteps != nil{
                DispatchQueue.main.async {
                    clouser(true)
                }
            }else{
                DispatchQueue.main.async {
                    clouser(false)
                }
            }
        }
    }
    
}
#endif

#if canImport(CoreBluetooth)
import CoreBluetooth
extension NK_PermissionManager: CBCentralManagerDelegate {
    /**
     蓝牙
     - parameters: action 权限结果闭包
     */
    func authBlueTooth(_ clouser:  @escaping AuthClouser){
        
        let mana = CBCentralManager()
        mana.delegate = self
        switch mana.state {
        case .unknown:
            print("unknown")
        case .resetting:
            print("resetting")
        case .unsupported:
            print("unsupported")
        case .unauthorized:
            print("unauthorized")
        case .poweredOff:
            print("poweredOff")
        case .poweredOn:
            print("poweredOn")
        @unknown default:
            break
        }

    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("unknown")
        case .resetting:
            print("resetting")
        case .unsupported:
            print("unsupported")
        case .unauthorized:
            print("unauthorized")
        case .poweredOff:
            print("poweredOff")
        case .poweredOn:
            print("poweredOn")
        @unknown default:
            break
        }
    }
}
#endif
